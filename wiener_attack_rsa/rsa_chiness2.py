import gmpy2
   
   
class RSAModuli:
  def __init__(self):
    self.a = 0
    self.b = 0
    self.m = 0
    self.i = 0
  def gcd(self, num1, num2):
           """
           This function os used to find the GCD of 2 numbers.
           :param num1:
           :param num2:
           :return:
           """
    if num1 < num2:
      num1, num2 = num2, num1
      while num2 != 0:
        num1, num2 = num2, num1 % num2
    return num1
  def extended_euclidean(self, e1, e2):
           """
           The value a is the modular multiplicative inverse of e1 and e2.
           b is calculated from the eqn: (e1*a) + (e2*b) = gcd(e1, e2)
           :param e1: exponent 1
           :param e2: exponent 2
           """
    self.a = gmpy2.invert(e1, e2)
    self.b = (float(self.gcd(e1, e2)-(self.a*e1)))/float(e2)
    def modular_inverse(self, c1, c2, N):
           """
           i is the modular multiplicative inverse of c2 and N.
           i^-b is equal to c2^b. So if the value of b is -ve, we
           have to find out i and then do i^-b.
           Final plain text is given by m = (c1^a) * (i^-b) %N
           :param c1: cipher text 1
           :param c2: cipher text 2
           :param N: Modulus
           """
      i = gmpy2.invert(c2, N)
      mx = pow(c1, self.a, N)
      my = pow(i, int(-self.b), N)
      self.m= mx * my % N
    def print_value(self):
      print("Plain Text: ", self.m)
   
   
def main():
  c = RSAModuli()
  N  =100000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000005000000000000000000000000000000000000000000000000267000000000000000000000000000000000000000000000062211
  c1 =48422511473521990375450896712899456383681349730838556629917526122372944503717161611542955850315226184440439228437399447916137445172653220231750594300230934894206506460735998916154380260461651842230328
  c2 =36935080566066809987512294593587165922689453239298365979934220773749515302662979395941264100472936036371948284005185898445333990657405117584787357541912376801751380784444605679758201704557692583862275
  e1 =5
  e2 =7
  c.extended_euclidean(e1, e2)
  c.modular_inverse(c1, c2, N)
  c.print_value()
    
if __name__ == '__main__':
    main()