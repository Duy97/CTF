import ContinuedFractions, Arithmetic, RSAvulnerableKeyGenerator
import time
import sys
import base64
import binascii
import gmpy
import sympy
import math
import fractions
import struct
sys.setrecursionlimit(1000000)
n = 10000000000000000000000000000000000001000000000000000000000000000000000000000000000000000001056739769590000000000000000000000000006876977059000000000000000000000000000000000000000000006794247018103252941
e= 3203449890631862429784387098639186217718279446801588954793327039406413404571860724495570135042580234804222431325521830314330004451526819839066981376681301773374420578027311643830854177449788451537218761
c = 4359030522973075720587278601741278446675538995555202673791456378351750902274976685956343503552190031486632797795997420849499709982230510048817085001379603109841297158448468838991422815547684902425140785
def hack_rsa(e,n):
	time.sleep(1)
	frac = ContinuedFractions.rational_to_contfrac(e,n)
	convergents = ContinuedFractions.convergents_from_contfrac(frac)
	for (k,d) in convergents:
		if k!=0 and (e*d-1) %k ==0:
			phi = (e*d -1) //k
			s = n - phi + 1
			discr = s*s - 4*n
			if(discr >=0):
				t = Arithmetic.is_perfect_square(discr)
				if t != -1 and (s+t) %2 ==0:
					print ' hacked'
					return d
def test_hack_rsa():
	print 'start'
	times = 5
	while(times>0):
		e,n,d = RSAvulnerableKeyGenerator.generatekeys(1024)
		print '(e,n) is '
		print 'd = '
		hacked_d = hack_rsa(e,n)
		if d== hacked_d:
			print ' hacked work'
		else:
			print 'hack fail'
		print d
		print hacked_d
		times = -1
if __name__ =='__main__':
	test_hack_rsa()














