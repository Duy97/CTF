def egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
        gcd = b
    return gcd, x, y

def main():

    p = 238324208831434331628131715304428889871
    q = 296805874594538235115008173244022912163
    e = 3
    ct = 29846947519214575162497413725060412546119233216851184246267357770082463030225

    # compute n
    n = p*q

    # Compute phi(n)
    phi = (p - 1) * (q - 1)

    # Compute modular inverse of e
    gcd, a, b = egcd(e, phi)
    d = a
    print a

    print( "n:  " + str(d) );

    # Decrypt ciphertext
    pt = pow(ct,d,n)
    print( "pt: " + str(pt) )

if __name__ == "__main__":
    main()