import string
import base64
def multiplyKey(ct, k):
    while len(k) < len(ct):
        k += k
    k = k[:len(ct)]
    return k
def decrypt(cipher,key):
	k = multiplyKey(cipher,key)
	plain=''
	for i in range(len(cipher)):
		plain += chr(ord(cipher[i]) ^ ord(k[i]))
	return plain
def check_keychar_validity(cipher,keychar,index=0,keylength=12):
	key = "\x00"*index +keychar+"\x00"*(keylength-index-1)
	pt = decrypt(cipher,key)
	for i in range(index,len(pt),keylength):
		if pt[i] not in base64str:
			return False
	return True
cipher = open("ciphertext.txt",'r').read().strip()
base64str = string.ascii_letters + string.digits+"=+/" +"\n"
key_chars = string.printable + string.whitespace
def fast_exhaustiver_srch(cipher,sub_block):
	key =""
	for i in key_chars:
		if check_keychar_validity(cipher,i,sub_block,12):
			key +=i
			for j in key_chars:
				if check_keychar_validity(cipher,j,sub_block+1,12):
					key +=j
					for k in key_chars:
						if check_keychar_validity(cipher,k,sub_block+2,12):
							key +=key_chars
							for l in key_chars:
								if check_keychar_validity(cipher,l,sub_block+3,12):
									key +=l
								else:
									continue
						else:
							continue
				else:
					continue
		else:
			continue
	return key
key = ''
for i in range(0,12,4):
	key +=fast_exhaustiver_srch(cipher,i)
print key
print decrypt(cipher,key)

















	