import string
 
def multiplyKey(ct, k):
    while len(k) < len(ct):
        k += k
    k = k[:len(ct)]
    return k
 
def decrypt(ciphertext, k):
    ciphertext = ciphertext.decode("hex")
    k = multiplyKey(ciphertext, k)
    plaintext = ""
    for i in range(len(ciphertext)):
        plaintext += chr(ord(ciphertext[i]) ^ ord(k[i]))
    return plaintext
 
def check_keychar_validity(ciphertext, keychar, index = 0, keylength = 12):
    key = "\x00"*index + keychar + "\x00"*(keylength-index-1)
    pt = decrypt(ciphertext, key)
    for i in range(index, len(pt), keylength):
        if pt[i] not in base64str:
            return False
    return True
 
# From the Index of Coincidence, we know that the most probable keylength is 12
# We also know that the keylength contains all printable characters excluding whitespaces
ciphertext = open("ciphertext.txt").read().strip()
base64str = string.ascii_letters + string.digits + "=+/" + "\n"
key_chars = string.printable + string.whitespace
 
def fast_exhaustive_srch(ciphertext, sub_block):
    key = ""
    for i in key_chars:
        if check_keychar_validity(ciphertext, i, sub_block, 12):
            key += i
            for j in key_chars:
                if check_keychar_validity(ciphertext, j, sub_block+1, 12):
                    key += j
                    for k in key_chars:
                        if check_keychar_validity(ciphertext, k, sub_block+2, 12):
                            key += k
                            for l in key_chars:
                                if check_keychar_validity(ciphertext, l, sub_block+3, 12):
                                    key += l
                                else:
                                    continue
                        else:
                            continue
                else:
                    continue
        else:
            continue
    return key
 
# Generating the entire key
key = ""
for i in range(0, 12, 4):
    key += fast_exhaustive_srch(ciphertext, i)
print key
print decrypt(ciphertext, key).decode("base64")