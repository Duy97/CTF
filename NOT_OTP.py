c1 = '38445d4e5311544249005351535f005d5d0c575b5e4f481155504e495740145f4c505c5c0e196044454817564d4e12515a5f4f12465c4a45431245430050154b4d4d415c560c4f54144440415f595845494c125953575513454e11525e484550424941595b5a4b'.decode("hex")
c2= '3343464b415550424b415551454b00405b4553135e5f00455f540c535750464954154a5852505a4b00455f5458004b5f430c575b58550c4e5444545e0056405d5f53101055404155145d5f0053565f59524c54574f46416c5854416e525e11506f485206554e51'.decode('hex')
def xor(a,b):
	return ''.join(chr(ord(x) ^ ord(y)) for x,y in zip(a,b))
def crib(m,c):
	for i in range(len(m)-len(c)+1):
		p = xor(m,"\x00"*i+c)

		s=''
		for e in p:
			if e in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ{}0123456789_ ?!.,'":
				s+=e
			else:
				s+='*'
	print s
plain = xor(c1,c2)
print plain
p = crib(plain,'easyctf{')
p = crib(plain,'plaintext used')
p = crib(c1,"to a sample of plaintext used in codebreaking")