from Crypto.PublicKey import RSA
from base64 import b64decode
from Crypto.Cipher import PKCS1_OAEP

#message I want to decipher
msg='e8oQDihsmkvjT3sZe+EE8lwNvBEsFegYF6+OOFOiR6gMtMZxxba/bIgLUD8pV3yEf0gOOfHuB5bC3vQmo7bE4PcIKfpFGZBA'

pub_key64 = 'MGQwDQYJKoZIhvcNAQEBBQADUwAwUAJJAMLLsk/b+SO2Emjj8Ro4lt5FdLO6WHMMvWUpOIZOIiPu63BKF8/QjRa0aJGmFHR1mTnG5Jqv5/JZVUjHTB1/uNJM0VyyO0zQowIDAQAB'

pub_keyDER = b64decode(pub_key64)
pub_key_obj = RSA.importKey(pub_keyDER)   #my weak public key
n = 593139068983
e = 65537L
d = 308700828273
private_key = RSA.construct((n, e, d))
decryptor = PKCS1_OAEP.new(private_key)
decrypted = decryptor.decrypt((str(msg)))
