import random
import fractions

#return a tuple(r,t) | n = r*2^t
def remove_even(n):
	if n==0:
		return (0,0)
	r=n
	t=0
	while (r&1)==0:
		t=t+1
		r=r >> 1
	return (r,t)
def get_root_one(x,k,n):
	(r,t) = remove_even(k)
	oldi = None
	i = pow(x,r,n)
	while i!=1:
		oldi =i
		i=(i*i) %n
	if oldi ==n-1:
		return None
	return oldi
def factor_rsa(e,d,n):
	k = e*d-1
	y =None
	while not y:
		x = random.randrange(2,n)
		y= get_root_one(x,k,n)
	p = fractions.gcd(y-1,n)
	q = n//p
	return (p,q)

