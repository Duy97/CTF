def xor_string(s,t):
	if isinstance(s,str):
		return "".join(chr(ord(a) ^ ord(b)) for a, b in  zip(s,t))
	else:
		return bytes([a^b for a,b in zip(s,t)])
message= 'Burning em, if you aint quick and nimbleI go crazy when I hear a cymbal'
print message
key = 'ICE'
cipher = xor_string(message.encode('utf8'), key.encode('utf8'))
print cipher
