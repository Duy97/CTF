from gmpy2 import * 

def f(x):
	""" function for pollard's rho """
	return x**2 + 1

def factor(n,b):
	""" Factor using Pollard's p-1 method """

	a = 2;
	for j in range(2,b):
		a = a**j % n
	
	d = gcd(a-1,n);
	print "d:",d,"a-1:",a-1
	if 1 < d < n: return d;
	else: return -1;

def factorRho(n,x_1):
	""" Factor using pollard's rho method """
	
	x = x_1;
	xp = f(x) % n
	p = gcd(x - xp,n)

	print "x_i's: {"
	while p == 1:
		print x,
		# in the ith iteration x = x_i and x' = x_2i
		x = f(x) % n
		xp = f(xp) % n
		xp = f(xp) % n
		p = gcd(x-xp,n)

	print "}"

	if p == n: return -1
	else: return p

def testFactor():
	
	print "Pollard's p-1 factoring"
	
	n = 16597712262800095098226130512282927179497011173406539443409876664765634654792363184411825406484837021057998483611151825837103388795480529002121502546121948928066281359031028270878297218661409934035886546020930964028780335737680348596910306513206117504108058925329858396067856573163592825383960058578681644875980180546847557812966223271765120162910255137784213094212309609759257353674875979882725784871783757837297703281196379092511265281287845023919390729682379344606965288462358145824117145872183931465422694130609005144944508184194344888497580577863328136806345023037939204996365581479722451260520977982655414777159
	s = 2
	d = -1

	print "n=%i, initial bound=%i" % (n,s)

	while s < n and d == -1:
		s +=1
		d = factor(n,s)
		print "Round %i = %i" % (s,d)

	if d == -1: print "No Factor could be found ..."
	else: print "%i has a factor of %i, with b=%i" % (n,d,s)

def testFactorRho():

	print "Pollard's Rho factoring" 
	n = 13493
	x_1 = 150

	print "n= %i, x_1= %i" % (n,x_1)
	
	p = factorRho(n,x_1)
	print "p=",p

testFactorRho();