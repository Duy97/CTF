// Server.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <conio.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <tchar.h>
#include "mylibrary.h"

#define SERVER_ADDR "127.0.0.1"
#define BUFF_SIZE 2048
#pragma comment (lib,"ws2_32")


account *create_acc(int n) {
	account *acc = new account[n];
	for (int i = 0; i < n; i++) {
		account new_acc;
		strcpy(new_acc.id, "chungnv");
		strcpy(new_acc.psw, "password");
		char c[100];
		sprintf(c, "%i", i);
		strcat(new_acc.id, c);
		strcat(new_acc.psw, c);
		new_acc.failed_login = 0;
		acc[i] = new_acc;
	}
	return acc;
}

int receiveData(SOCKET, char *, int, int);
int sendData(SOCKET, char *, int, int);
void prc_mess(state_client *, char *, account *);
account *acc = create_acc(10);


int _tmain(int argc, char *argv[]) {

	//Step 1: Initiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData)) {
		printf("Version is not supported!\n");
	}

	//Step 2: Construct socket
	SOCKET listenSock;
	listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	DWORD nEvents = 0;
	DWORD i, index;
	state_client st_client[WSA_MAXIMUM_WAIT_EVENTS];
	SOCKET connSock;
	for (int i = 0; i < WSA_MAXIMUM_WAIT_EVENTS; i++) {
		st_client[i].connSock = 0;
		st_client[i].state = "Un-authenticated";
	}
	WSAEVENT events[WSA_MAXIMUM_WAIT_EVENTS], newEvent;
	WSANETWORKEVENTS sockEvent;
	newEvent = WSACreateEvent();

	// Associate event types FD_ACCEPT and FD_CLOSE
	// with the listening socket and newEvent
	WSAEventSelect(listenSock, newEvent, FD_ACCEPT | FD_CLOSE);
	st_client[0].connSock = listenSock;
	events[0] = newEvent;
	nEvents++;

	//Step 3: Bind address to socket
	sockaddr_in serverAddr, clientAddr;
	int clientAddrlen = sizeof(clientAddr);
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

	if (argc == 3) {
		if (strcmp(argv[1], "-p") == 0) {
			if (check_number(argv[2])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[2]));
				//Bind
				if (bind(listenSock, (sockaddr*)&serverAddr, sizeof(sockaddr))) {
					printf("ERROR: Can not bind this address!");
					return 0;
				}
				printf("Server started in [%s:%s]", SERVER_ADDR, argv[2]);
			}
			else {
				printf("ERROR : Command line parameter is wrong!");
				return 0;
			}
		}
		else {
			printf("ERROR : Command line parameter is wrong!");
			return 0;
		}
	}
	else {
		printf("ERROR : Command line parameter is wrong!");
		return 0;
	}
	
	//Step 4: Listen request from client
	if (listen(listenSock, 10)) {
		printf("ERROR! Can not listen!");
		_getch();
		return 0;
	}

	//Step 5: Communicate with clients
	char rcvBuff[BUFF_SIZE], sendBuff[BUFF_SIZE];
	while (1) {
		//wait for network events on all socket
		index = WSAWaitForMultipleEvents(nEvents, events, FALSE,
			WSA_INFINITE, FALSE);
		if (index == WSA_WAIT_FAILED) {
			printf("index = Failed");
			break;
		}
		index = index - WSA_WAIT_EVENT_0;
		// Iterate through all events and enumerate
		// if the wait does not fail.
		for (i = index; i < nEvents; i++) {
			index = WSAWaitForMultipleEvents(1, &events[i],
				FALSE, 500, FALSE);
			if (index != WSA_WAIT_FAILED && index != WSA_WAIT_TIMEOUT) {
				//index = index � WSA_WAIT_EVENT_0;
				WSAEnumNetworkEvents(st_client[i].connSock, events[i],
					&sockEvent);
				if (sockEvent.lNetworkEvents & FD_ACCEPT) {
					if (sockEvent.iErrorCode[FD_ACCEPT_BIT] != 0) {
						printf("Error!");
						break;
					}
					printf("\nAccepting...");
					connSock = accept(listenSock, (sockaddr*)&clientAddr, &clientAddrlen);
					newEvent = WSACreateEvent();
					if (WSAEventSelect(connSock, newEvent, FD_READ | FD_CLOSE) == SOCKET_ERROR) {
						printf("EventSelect ERROR!");
					}
					int j;
					for (j = 0; j < WSA_MAXIMUM_WAIT_EVENTS; j++) {
						if (st_client[j].connSock == 0) {
							st_client[j].connSock = connSock;
							events[j] = newEvent;
							if (j == nEvents) nEvents++;
							printf("\nOne accept commplete in %d!", j);
							break;
						}
					}
					if (j == WSA_MAXIMUM_WAIT_EVENTS) {
						printf("To many socket!");
					}
				}
				if (sockEvent.lNetworkEvents & FD_READ) {
					if (sockEvent.iErrorCode[FD_READ_BIT] != 0) {
						printf("Error!");
						break;
					}
					receiveData(st_client[i].connSock, rcvBuff, BUFF_SIZE, 0);
					prc_mess(&st_client[i], rcvBuff, acc);
				}
				if (sockEvent.lNetworkEvents & FD_WRITE) {
					//...
				}
				if (sockEvent.lNetworkEvents & FD_CLOSE) {
					closesocket(st_client[i].connSock);
					printf("\nOne client closed!");
					st_client[i].connSock = 0;
					st_client[i].state = "Un-authenticated";
				}
				//reset event
				WSAResetEvent(events[i]);
			}
		}//end for
	}//end while
	_getch();
	return 0;
}

/*The process message function: process message and send answer to client*/
void prc_mess(state_client *state_cl, char *buff, account *acc) {
	state_client st_cl;
	memcpy(&st_cl, state_cl, sizeof(state_client));
	message mes;
	memcpy(&mes, buff, sizeof(message));
	switch (mes.TYPE)
	{
	case 1: { //Specify user
		if (strcmp(st_cl.state, "Un-authenticated") == 0) {
			int check = check_acc(acc, mes.mes, 10);
			if (check == 0) {//not found id
				sendData(st_cl.connSock, "11", 2, 0);
			}
			else {//found id
				sendData(st_cl.connSock, "01", 2, 0);
				st_cl.state = "Specified-user";
				memcpy(&st_cl.acc, &acc[check - 1], sizeof(account));
			}
		}
		else {
			sendData(st_cl.connSock, "21", 2, 0);
		}
		break;
	}
	case 2: {	//Mathed password
		if (strcmp(st_cl.state, "Specified-user") == 0) {
			if (check_pass(mes.mes, st_cl.acc)) {
				sendData(st_cl.connSock, "02", 2, 0);
				st_cl.state = "Authenticated";
			}
			else {
				sendData(st_cl.connSock, "12", 2, 0);
			}
		}
		else {
			sendData(st_cl.connSock, "22", 2, 0);

		}
		break;
	}
	case 3: {	//LOGOUT
		if (strcmp(st_cl.state, "Authenticated") == 0) {
			sendData(st_cl.connSock, "03", 2, 0);
			st_cl.state = "Un-authenticated";
		}
		else {
			sendData(st_cl.connSock, "13", 2, 0);
		}
		break;
	}
	default:
		break;
	}
	memcpy(state_cl, &st_cl, sizeof(state_client));
}

/*The recv() wrapper function*/
int receiveData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = recv(s, buff, size, flags);
	if (n <= 0) {
		//printf("\nOne client disconnected.");
	}
	else {
		buff[n] = '\0';
		//printf("\nreceive one message!");
	}
	return n;
}


/*The send() wrapper function*/
int sendData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = send(s, buff, size, flags);
	//if (n <= 0) printf("\nsend error.");
	return n;
}