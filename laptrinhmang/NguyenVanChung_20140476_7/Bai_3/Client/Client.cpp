#include <stdio.h>
#include <conio.h>
#include <WinSock2.h>
#include <tchar.h>
#include <WS2tcpip.h>
#include "mylibrary.h"
#define BUFF_SIZE 2048

#pragma comment (lib,"WS2_32")

int main(int argc,char *argv[]){
	//Step1: Initiate Winsock
	WSAData wsaData;
	WORD wVersion = MAKEWORD(2,2);
	WSAStartup(wVersion, &wsaData);

	//Step2: Contruct socket
	SOCKET client;
	client = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	//(optional) Set time-out for receiving
	int tv = 15000; //Time-out interval: 10000ms
	setsockopt(client, SOL_SOCKET, SO_RCVTIMEO, (const char*)(&tv), sizeof(int));
	//Step3: Specify server address
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	
	//Process command line parameters
	if (argc == 5) {
		if (strcmp(argv[1], "-a") == 0 && strcmp(argv[3], "-p") == 0) {
			if (check_number(argv[4]) && check_ip(argv[2])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[4]));
				serverAddr.sin_addr.s_addr = inet_addr(argv[2]);
			}
			else {
				printf("ERROR : Wrong Value!");
				return 0;
			}
		}
		else {
			printf("ERROR : Command line parameter is wrong!");
			return 0;
		}
	}
	else {
		printf("ERROR : Command line parameter is wrong!");
		return 0;
	}
	//Step 4: Request to connect server
	if (connect(client, (sockaddr *)&serverAddr, sizeof(serverAddr))) {
		printf("Error! Cannot connect server [%s:%s]: %d", argv[2], argv[4], WSAGetLastError());
		_getch();
		return 0;
	}
	printf("Connected server in address [%s:%s]!\n", argv[2], argv[4]);
	
	char *buff = new char[BUFF_SIZE];
	int ret;
	while (true) {
		printf("Send to server: ");
		gets_s(buff, BUFF_SIZE);
		if (check_input(buff) == 0) break;
		else if (check_input(buff) == -1) {
			printf("Input have only letter don't have digit or special characters!\n");
			continue;
		}
		ret = send(client, buff, strlen(buff), 0);
		if (ret == SOCKET_ERROR) {
			printf("Can not send to server!\n");
			continue;
		}
		ret = recv(client,buff,BUFF_SIZE,0);
		if (ret == SOCKET_ERROR) {
			if (WSAGetLastError() == WSAETIMEDOUT)
				printf("Time-out!\n");
			else printf("Error! Cannot receive answer.\n");
			continue;
		}
		buff[ret] = 0;
		printf("Receive from server: %s\n", buff);
	}//end while
	printf("Communication complete!");
	_getch();
	return 0;
}