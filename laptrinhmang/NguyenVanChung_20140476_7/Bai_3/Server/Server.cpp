#include <stdio.h>
#include <conio.h>
#include <WinSock2.h>
#include <tchar.h>
#include <WS2tcpip.h>
#include "mylibrary.h"

#define BUFF_SIZE 2048
#define SERVER_ADDR "127.0.0.1"

#pragma comment (lib,"WS2_32")

char *senbuf(char *);

int main(int argc, char *argv[]) {
	//Step1: Initiate Winsock
	WSAData wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	WSAStartup(wVersion, &wsaData);

	SOCKET connSocket, listenSocket;
	WSAEVENT events[WSA_MAXIMUM_WAIT_EVENTS];
	WSANETWORKEVENTS sockEvent;
	SOCKET client[WSA_MAXIMUM_WAIT_EVENTS];
	for (int i = 0; i < WSA_MAXIMUM_WAIT_EVENTS; i++) {
		client[i] = 0;
	}
	DWORD flags = 0, nEvents = 0, bytesTransferred = 0, bytesRcv = 0, bytesSend = 0;
	WSAOVERLAPPED acceptOverlapped[WSA_MAXIMUM_WAIT_EVENTS];
	WSAEVENT newEvent;
	newEvent = WSACreateEvent();
	listenSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	WSAEventSelect(listenSocket, newEvent, FD_ACCEPT | FD_CLOSE);
	client[0] = listenSocket;
	events[0] = newEvent;
	nEvents++;
	sockaddr_in serverAddr, clientAddr;
	int clientAddrlen = sizeof(clientAddr);
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);
	// Bind address to socket
	if (argc == 3) {
		if (strcmp(argv[1], "-p") == 0) {
			if (check_number(argv[2])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[2]));
				//Bind
				if (bind(listenSocket, (sockaddr*)&serverAddr, sizeof(sockaddr))) {
					printf("ERROR: Can not bind this address!");
					return 0;
				}
				printf("Server started in [%s:%s]", SERVER_ADDR, argv[2]);
			}
			else {
				printf("ERROR : Command line parameter is wrong!");
				return 0;
			}
		}
		else {
			printf("ERROR : Command line parameter is wrong!");
			return 0;
		}
	}
	else {
		printf("ERROR : Command line parameter is wrong!");
		return 0;
	}
	// Listen request from client
	if (listen(listenSocket, 10)) {
		printf("\nERROR! Can not listen!");
		_getch();
		return 0;
	}
	WSABUF rcvBuf, sendBuf;
	rcvBuf.buf = new char[BUFF_SIZE];
	rcvBuf.len = BUFF_SIZE;
	int i;
	// Process overlapped receives on the socket
	while (TRUE) {
		DWORD index;
		// Step 5: Wait for the accept event and the overlapped I/O call to complete
		index = WSAWaitForMultipleEvents(nEvents, events, FALSE,
			WSA_INFINITE, FALSE);
		if (index == WSA_WAIT_FAILED) {
			printf("index Failed");
			break;
		}
		index = index - WSA_WAIT_EVENT_0;
		// Iterate through all events and enumerate
		// if the wait does not fail.
		int check = nEvents;
		for (i = index; i < check; i++) {
			if (client[i] == 0) continue;
			if (i == 0) {
				index = WSAWaitForMultipleEvents(1, &events[i],
					FALSE, 500, FALSE);
				if (index != WSA_WAIT_FAILED && index != WSA_WAIT_TIMEOUT) {
					WSAEnumNetworkEvents(client[i], events[i],
						&sockEvent);
					if (sockEvent.lNetworkEvents & FD_ACCEPT) {
						if (sockEvent.iErrorCode[FD_ACCEPT_BIT] != 0) {
							printf("Error!");
							break;
						}
						printf("\nAccepting...");
						connSocket = accept(listenSocket, (sockaddr*)&clientAddr, &clientAddrlen);
						newEvent = WSACreateEvent();
						int j;
						for (j = 1; j < WSA_MAXIMUM_WAIT_EVENTS; j++) {
							if (client[j] == 0) {
								client[j] = connSocket;
								events[j] = newEvent;
								if (j == nEvents) nEvents++;
								printf("\nOne accept commplete in %d!", j);
								ZeroMemory(&acceptOverlapped[j], sizeof(WSAOVERLAPPED));
								acceptOverlapped[j].hEvent = events[j];
								if (WSARecv(connSocket, &rcvBuf, 1, &bytesRcv, &flags, &acceptOverlapped[j], NULL) == SOCKET_ERROR)
								{
									if (WSAGetLastError() != WSA_IO_PENDING)
									{
										printf("\nCan not call I/O function!");
									}
								}
								break;
							}
						}
						if (j == WSA_MAXIMUM_WAIT_EVENTS) {
							printf("To many socket!");
						}
					}
					if (sockEvent.lNetworkEvents & FD_CLOSE) {
						closesocket(client[i]); //close listenSocket
					}
					WSAResetEvent(events[i]);
				}
			}
			else {
				index = WSAWaitForMultipleEvents(1, &events[i],
					FALSE, 200, FALSE);
				if (index != WSA_WAIT_FAILED && index != WSA_WAIT_TIMEOUT) {
					// Step 6: Determine the status of the overlapped request
					WSAGetOverlappedResult(client[i], &acceptOverlapped[i],
						&bytesTransferred, FALSE, &flags);
					printf("\nclient[%d]", i);
					// First check to see whether the peer has closed
					// the connection, and if so, close the socket
					if (bytesTransferred == 0) {
						//...
						closesocket(client[i]);
						client[i] = 0;
						printf("\nClient[%d] closed!", i);
						WSAResetEvent(events[i]);
						ZeroMemory(&acceptOverlapped[i], sizeof(WSAOVERLAPPED));
						continue;
					}
					//do something with the received data...
					if (acceptOverlapped[i].InternalHigh != 0) {
						sendBuf.buf = rcvBuf.buf;
						sendBuf.buf[acceptOverlapped[i].InternalHigh] = 0;
						printf("\n%s", sendBuf.buf);
						sendBuf.len = acceptOverlapped[i].InternalHigh;
						if (send(client[i], senbuf(sendBuf.buf), sendBuf.len, 0) == SOCKET_ERROR) {
							printf("\nCan not send!");
						}
					}
					// Step 7: Reset the signaled event
					WSAResetEvent(events[i]);
					ZeroMemory(&acceptOverlapped[i], sizeof(WSAOVERLAPPED));
					acceptOverlapped[i].hEvent = events[i];
					// Step 8: Call I/O function again ...
					if (WSARecv(client[i], &rcvBuf, 1, &bytesRcv, &flags, &acceptOverlapped[i], NULL) == SOCKET_ERROR)
					{
						if (WSAGetLastError() != WSA_IO_PENDING)
						{
							printf("\nCan not call I/O function!");
						}
					}
				}
			}
		}
	} // end while
	_getch();
	return 0;
}

char *senbuf(char *sendbuff) {
	byte *a = (byte*)strrev(sendbuff);
	for (int i = 0; i < strlen(sendbuff); i++) {
		a[i] = a[i] ^ 32;
	}
	return (char*)a;
}
