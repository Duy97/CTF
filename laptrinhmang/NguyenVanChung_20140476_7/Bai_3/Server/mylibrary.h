#pragma once
#include<stdio.h>
#include<conio.h>
#include<tchar.h>
#include<ctype.h>

bool check_number(char *buff) {
	int check = 0;
	for (int i = 0; i < strlen(buff); i++) {
		if (!isdigit(buff[i])) {
			check = 1;
			break;
		}
	}
	if (check == 0) return true;
	else return false;
}
