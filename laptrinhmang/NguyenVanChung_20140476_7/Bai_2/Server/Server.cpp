// Server.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <conio.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <tchar.h>
#include "mylibrary.h"

#define SERVER_ADDR "127.0.0.1"
#define BUFF_SIZE 1028
#pragma comment (lib,"ws2_32")


void sendER(SOCKET);
int receiveData(SOCKET, char *, int, int);
int sendData(SOCKET, char *, int, int);
void prc_mess(state_client *, char *);
int send_to_client(state_client *);


int _tmain(int argc, char *argv[]) {

	//Step 1: Initiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData)) {
		printf("Version is not supported!\n");
	}

	//Step 2: Construct socket
	SOCKET listenSock;
	listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	int ret;
	DWORD nEvents = 0;
	DWORD i, index;
	state_client st_client[WSA_MAXIMUM_WAIT_EVENTS];
	SOCKET connSock;
	for (int i = 0; i < WSA_MAXIMUM_WAIT_EVENTS; i++) {
		//Initialize value for st_client
		st_client[i].connSock = 0;
		st_client[i].state = "receive";
		st_client[i].key = 0;
		st_client[i].fp = NULL;
		st_client[i].tem_file = "";
		st_client[i].result_file = "";
	}
	WSAEVENT events[WSA_MAXIMUM_WAIT_EVENTS], newEvent;
	WSANETWORKEVENTS sockEvent;
	newEvent = WSACreateEvent();

	// Associate event types FD_ACCEPT and FD_CLOSE
	// with the listening socket and newEvent
	WSAEventSelect(listenSock, newEvent, FD_ACCEPT | FD_CLOSE);
	st_client[0].connSock = listenSock;
	events[0] = newEvent;
	nEvents++;

	//Step 3: Bind address to socket
	sockaddr_in serverAddr, clientAddr;
	int clientAddrlen = sizeof(clientAddr);
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

	if (argc == 3) {
		if (strcmp(argv[1], "-p") == 0) {
			if (check_number(argv[2])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[2]));
				//Bind
				if (bind(listenSock, (sockaddr*)&serverAddr, sizeof(sockaddr))) {
					printf("ERROR: Can not bind this address!");
					return 0;
				}
				printf("Server started in [%s:%s]", SERVER_ADDR, argv[2]);
			}
			else {
				printf("ERROR : Command line parameter is wrong!");
				return 0;
			}
		}
		else {
			printf("ERROR : Command line parameter is wrong!");
			return 0;
		}
	}
	else {
		printf("ERROR : Command line parameter is wrong!");
		return 0;
	}

	//Step 4: Listen request from client
	if (listen(listenSock, 10)) {
		printf("ERROR! Can not listen!");
		_getch();
		return 0;
	}

	//Step 5: Communicate with clients
	char rcvBuff[BUFF_SIZE], sendBuff[BUFF_SIZE];
	while (1) {
		//wait for network events on all socket
		index = WSAWaitForMultipleEvents(nEvents, events, FALSE,
			WSA_INFINITE, FALSE);
		if (index == WSA_WAIT_FAILED) {
			printf("index Failed");
			break;
		}
		index = index - WSA_WAIT_EVENT_0;
		// Iterate through all events and enumerate
		// if the wait does not fail.
		for (i = index; i < nEvents; i++) {
			index = WSAWaitForMultipleEvents(1, &events[i],
				FALSE, 500, FALSE);
			if (index != WSA_WAIT_FAILED && index != WSA_WAIT_TIMEOUT) {
				//index = index � WSA_WAIT_EVENT_0;
				WSAEnumNetworkEvents(st_client[i].connSock, events[i],
					&sockEvent);
				if (sockEvent.lNetworkEvents & FD_ACCEPT) {
					if (sockEvent.iErrorCode[FD_ACCEPT_BIT] != 0) {
						printf("Error!");
						break;
					}
					printf("\nAccepting...");
					connSock = accept(listenSock, (sockaddr*)&clientAddr, &clientAddrlen);
					char *id = inet_ntoa(clientAddr.sin_addr);
					int port = (int)ntohs(clientAddr.sin_port);
					char *tem_file = new char[200];
					sprintf(tem_file, "../File_Of_Server/itemporary%i.txt", port);
					char *result_file = new char[200];
					sprintf(result_file, "../File_Of_Server/result%i.txt", port);
					newEvent = WSACreateEvent();
					if (WSAEventSelect(connSock, newEvent, FD_READ | FD_CLOSE) == SOCKET_ERROR) {
						printf("EventSelect ERROR!");
					}
					int j;
					for (j = 0; j < WSA_MAXIMUM_WAIT_EVENTS; j++) {
						if (st_client[j].connSock == 0) {
							st_client[j].connSock = connSock;
							st_client[j].tem_file = tem_file; //add url item file for client
							st_client[j].result_file = result_file; // add url result file for client
							events[j] = newEvent;
							if (j == nEvents) nEvents++;
							printf("\nOne accept commplete in %d!", j);
							break;
						}
					}
					if (j == WSA_MAXIMUM_WAIT_EVENTS) {
						printf("To many socket!");
					}
				}
				if (sockEvent.lNetworkEvents & FD_READ) {
					if (sockEvent.iErrorCode[FD_READ_BIT] != 0) {
						printf("Error!");
						break;
					}
					newEvent = WSACreateEvent();
					if (WSAEventSelect(st_client[i].connSock, newEvent, FD_READ | FD_CLOSE) == SOCKET_ERROR) {
						printf("EventSelect ERROR!");
					}
					if (receiveData(st_client[i].connSock, rcvBuff, BUFF_SIZE, 0) > 0) {
						if (strcmp(rcvBuff, "OK") != 0) {
							prc_mess(&st_client[i], rcvBuff);
							printf("\n%s", st_client[i].state);
						}
						if (strcmp(st_client[i].state, "sendfile") == 0) {
							ret = send_to_client(&st_client[i]);
							if (ret == 0) {
								printf("Send file complete!");
								st_client[i].state = "receive"; // change state to receive file
								close_file(st_client[i].fp); // close file point
								remove_file(st_client[i].tem_file);//Remove item file
								remove_file(st_client[i].result_file);//Remove result file
							}
						}
					}
					events[i] = newEvent;
					continue;
				}
				if (sockEvent.lNetworkEvents & FD_WRITE) {
					//..
				}
				if (sockEvent.lNetworkEvents & FD_CLOSE) {
					printf("\nOne client closed!");
					//Close socket and reset value of st_client
					close_file(st_client[i].fp); //close file point
					closesocket(st_client[i].connSock); //close socket
					remove_file(st_client[i].tem_file); //Remove item file
					remove_file(st_client[i].result_file); //Remove result file
					//Reset value of st_client
					st_client[i].connSock = 0;
					st_client[i].key = 0;
					st_client[i].state = "receive";
					close_file(st_client[i].fp);
					st_client[i].tem_file = "";
					st_client[i].result_file = "";
					nEvents--;
				}
				//reset event
				WSAResetEvent(events[i]);
			}
		}//end for
	}//end while
	_getch();
	return 0;
}

/*The process message function: process message and send answer to client*/
void prc_mess(state_client *st_client, char *buff) {
	state_client st_cl;
	memcpy(&st_cl, st_client, sizeof(state_client));
	message mes;
	memcpy(&mes, buff, sizeof(message));
	switch (atoi(mes.Opcode)) {
	case 0: {
		//create itemporary file and result file
		create_file(st_cl.tem_file);
		create_file(st_cl.result_file);
		st_cl.fp = fopen(st_cl.tem_file, "wb");
		//save key
		st_cl.key = atoi(mes.Payload);
		//change state
		st_cl.state = "ENCODE";
		break;
	}
	case 1: {
		//create itemporary file and result file
		create_file(st_cl.tem_file);
		create_file(st_cl.result_file);
		st_cl.fp = fopen(st_cl.tem_file, "wb");
		//save key
		st_cl.key = atoi(mes.Payload);
		//change state
		st_cl.state = "DECODE";
		break;
	}
	case 2: {
		switch (mes.length)
		{
		case 0: {
			//end receive file
			close_file(st_cl.fp);
			close_file(st_client[0].fp);
			if (strcmp(st_cl.state, "ENCODE") == 0) {//ENCODE file
				if (encode_file(st_cl.result_file, st_cl.tem_file, st_cl.key)) {
					st_cl.state = "sendfile";
				}
				else sendER(st_cl.connSock);
			}
			else {//DECODE file
				if (encode_file(st_cl.result_file, st_cl.tem_file, (-st_cl.key))) {
					st_cl.state = "sendfile";
				}
				else sendER(st_cl.connSock);
			}
			st_cl.fp = fopen(st_cl.result_file, "rb");//open result file
			break;
		}
		default: {
			//save_file
			fwrite(mes.Payload, 1, (int)mes.length, st_cl.fp);
			fflush(st_cl.fp);
			break;
		}
		}
	}
	case 3: {
		break;
	}
	default:
		break;
	}
	memcpy(st_client, &st_cl, sizeof(state_client));
}

/*send file to server*/
int send_to_client(state_client *st_client) {
	state_client st_cl;
	memcpy(&st_cl, st_client, sizeof(state_client));
	message mes;
	memcpy(mes.Opcode, "2", 1);
	int read;
	read = fread_s(mes.Payload, 1024, sizeof(char), 1024, st_cl.fp);
	mes.length = read;
	sendData(st_cl.connSock, (char *)&mes, 1028, 0);
	memcpy(st_client, &st_cl, sizeof(state_client));
	return read;
}

//Send message notify Error
void sendER(SOCKET connsock) {
	message meserr;
	int ret;
	memcpy(meserr.Opcode, "3", 1);
	ret = send(connsock, (char *)&meserr, sizeof(message), 0);
	if (ret == SOCKET_ERROR) {
		//printf("ERROR: %", WSAGetLastError());
	}
	return;
}


/*The recv() wrapper function*/
int receiveData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = recv(s, buff, size, flags);
	if (n <= 0) {
		printf("ERROR");
	}
	else {
		if (n < BUFF_SIZE) buff[n] = '\0';
	}
	return n;
}

/*The send() wrapper function*/
int sendData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = send(s, buff, size, flags);
	if (n <= 0) printf("\nsend error.");
	return n;
}