#include<stdio.h>
#include<WinSock2.h>
#include<WS2tcpip.h>
#include<tchar.h>
#include "mylibrary.h"
#define BUFF_SIZE 2048

#pragma comment (lib,"Ws2_32")

void communicate(SOCKET client);
void slt_request(SOCKET clietn);
void sendER(SOCKET client);
void sendfl(SOCKET client);
void receive_file(SOCKET client, FILE *fp);
int send_file(SOCKET client,char *url_file);

int _tmain(int argc, char *argv[]) {

	//Step 1: Initiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData)) {
		printf("Version is not support!\n");
		return 0;
	}
	//Step 2: Contruct socket
	SOCKET client;
	client = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	//(optional) Set time-out for receiving
	int tv = 15000; //Time-out interval: 10000ms
	setsockopt(client, SOL_SOCKET, SO_RCVTIMEO, (const char*)(&tv), sizeof(int));


	//Step 3: Specify server address
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;

	//Process command line parameters
	if (argc == 5) {
		//check command line parameters
		if (strcmp(argv[1], "-a") == 0 && strcmp(argv[3], "-p") == 0) {
			//check ip and port
			if (check_number(argv[4]) && check_ip(argv[2])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[4]));
				serverAddr.sin_addr.s_addr = inet_addr(argv[2]);
			}
			else {
				printf("ERROR : Wrong Value!");
				return 0;
			}
		}
		else {
			printf("ERROR : Command line parameter is wrong!");
			return 0;
		}
	}
	else {
		printf("ERROR : Command line parameter is wrong!");
		return 0;
	}

	//Step 4: Request to connect server

	//connect to server
	if (connect(client, (sockaddr *)&serverAddr, sizeof(serverAddr))) {
		printf("Error! Cannot connect server [%s:%s]: %d", argv[2], argv[4], WSAGetLastError());
		_getch();
		return 0;
	}

	//connect success
	printf("Connected server in address [%s:%s]!\n", argv[2], argv[4]);


	//Step 5: Communicate with server
	
	//function communicate
	communicate(client);

	//Close Socket
	closesocket(client);

	//Terminate WinSock
	WSACleanup();

	//end client
	_getch();
}

void communicate(SOCKET client){
	slt_request(client);
	return;
}

void slt_request(SOCKET client) {
	int check = 0;
	char *request = new char[50];
	int ret;

	while (check == 0) {

		//Input request encode or decode
		printf("Select Request [ENCODE or DECODE OR EXIT] : ");
		gets_s(request, 50);
		if (strcmp(request, "ENCODE") == 0) {
			//send message encode to server
			message mes;
			int t_t = 0;
			//input key
			while (t_t == 0) {
				printf("Input Key: ");
				gets_s(mes.Payload, 1024);
				if (check_number(mes.Payload) && atoi(mes.Payload) != 0) {
					t_t = 1;
				}
				else {
					printf("Wrong! Ket must is integer and != 0 !\n");
				}
			}//end while
			memcpy(mes.Opcode, "0", 1);
			ret = send(client, (char*)&mes, sizeof(message), 0);
			if (ret == SOCKET_ERROR) {
				printf("ERROR: %s", WSAGetLastError());
				return;
			}
			char *url_file = new char[200];
			int test = 0;

			//input url file
			while (test == 0) {
				printf("Url File: ");
				gets_s(url_file, 200);
				if (check_file(url_file)) {
					test = 1;
				}
				else {
					printf("File Not Exist!\n");
				}
			}//end while

			//send file to server
			int sent_size = send_file(client, url_file);
			printf("Sent: %d byte!\n", sent_size);

			//Get url encode file
			char *file_name = get_filename(url_file);
			char encode_file[200] = "../File_Of_Client/encode-";
			strcat(encode_file, file_name);

			//receive file from server
			FILE *fp = fopen(encode_file, "wb");
			receive_file(client, fp);
			fclose(fp);
			printf("Size of file receive %d byte\n", getfilesize(encode_file));
			check = 1;
		}
		else {
			if (strcmp(request, "DECODE") == 0) {
				//send message decode to server
				message mes;
				int t_t = 0;

				//input key
				while (t_t == 0) {
					printf("Input Key: ");
					gets_s(mes.Payload, 1024);
					if (check_number(mes.Payload) && atoi(mes.Payload) != 0) {
						t_t = 1;
					}
					else {
						printf("Wrong! Ket must is integer and != 0 !\n");
					}
				}//end while
				memcpy(mes.Opcode, "1", 1);
				ret = send(client, (char*)&mes, sizeof(message), 0);
				if (ret == SOCKET_ERROR) {
					printf("ERROR: %s", WSAGetLastError());
					return;
				}
				char *url_file = new char[200];
				int test = 0;

				//input url file
				while (test == 0) {
					printf("Url File: ");
					gets_s(url_file, 200);
					if (check_file(url_file)) {
						test = 1;
					}
					else {
						printf("File Not Exist!\n");
					}
				}//end while

				//send file to server
				int sent_size = send_file(client, url_file);
				printf("Sent: %d byte!\n", sent_size);
				char *file_name = get_filename(url_file);
				char decode_file[200] = "../File_Of_Client/decode-";
				strcat(decode_file, file_name);

				//receive file from server
				FILE *fp = fopen(decode_file, "wb");
				receive_file(client, fp);
				fclose(fp);
				printf("Size of file receive %d byte\n", getfilesize(decode_file));
				check = 1;
			}
			else {
				if (strcmp(request, "EXIT") == 0) {
					check = 1;
				}
				else {
					//select request false
					printf("SORRY, You can select only ENCODE or DECODE or EXIT. Please Try Again!\n");
				}
			}
		}
	}//end while
}


//function send file
int send_file(SOCKET client, char *url_file) {
	int ret;
	int size = getfilesize(url_file);
	//open file
	FILE *fr = fopen(url_file, "rb");
	//send file
	if (size < 1024) { //small file
		message mesf;
		memcpy(mesf.Opcode, "2", 1);
		memcpy(&mesf.length,&size,2);
		char *buffer = new char[size];
		fread_s(buffer, size, sizeof(char), size, fr);
		memcpy(mesf.Payload, buffer, size);
		ret = send(client, (char *)&mesf, sizeof(message), 0);
		if (ret == SOCKET_ERROR) {
			printf("ERROR: %", WSAGetLastError());
			sendER(client);
			fclose(fr);
			return 0;
		}
		sendfl(client);
		fclose(fr);
		return size;
	}
	else {//big file
		int count_byte = 0;
		while (count_byte < size) {
			message mesf;
			memcpy(mesf.Opcode, "2", 1);
			char *buffer = new char[1024];
			int read = fread_s(buffer, 1024, sizeof(char), 1024, fr);
			mesf.length =(u_short)read;
			memcpy(mesf.Payload, buffer, read);
			ret = send(client, (char *)&mesf, sizeof(message), 0);
			if (ret == SOCKET_ERROR) {
				printf("ERROR: %", WSAGetLastError());
				sendER(client);
				fclose(fr);
				return 0;
			}
			count_byte += read;
			free(buffer);
		}
		//end send file
		sendfl(client); //send message notify finish
		fclose(fr);
		return count_byte;
	}
}

//function receive file
void receive_file(SOCKET client,FILE *fp) {
	int check1 = 0,ret;
	while (check1 == 0) {
		char *buffer = new char[1028];
		ret = recv(client, buffer, 1028, 0);
		if (ret == SOCKET_ERROR) {
			printf("Can not receive!\n");
			check1 = 1;
		}
		message *mes1 = new message[1];
		memcpy(mes1, buffer, sizeof(message));
		switch (atoi(mes1[0].Opcode))
		{
		case 2: {
			switch (mes1[0].length)
			{
			case 0: {
				//end receive file
				printf("Receive file from Server: Complete!\n");
				check1 = 1;
				break;
			}
			default: {
				//save_file
				send(client, "OK", 2, 0);
				fwrite((byte*)mes1[0].Payload, 1, (int)mes1[0].length, fp);
				fflush(fp);
				break;
			}
			}
			break;
		}
		case 3: {
			//receive Error from server
			printf("Receive From Server: ERROR.\n");
			check1 = 1;
			break;

		}
		default:
			break;
		}
		free(mes1);
		free(buffer);
	}//end while
}


//Send message ERROR
void sendER(SOCKET client) {
	message meserr;
	int ret;
	memcpy(meserr.Opcode, "3", 1);
	ret = send(client, (char *)&meserr, sizeof(message), 0);
	if (ret == SOCKET_ERROR) {
		printf("ERROR: %", WSAGetLastError());
	}
}


//Send message notify finish
void sendfl(SOCKET client) {
	message mesfl;
	int ret;
	mesfl.length = 0;
	memcpy(mesfl.Opcode, "2", 1);
	ret = send(client, (char*)&mesfl, sizeof(message), 0);
	if (ret == SOCKET_ERROR) {
		printf("ERROR: %", WSAGetLastError());
	}
	return;
}