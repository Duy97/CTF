#pragma once
#include<stdio.h>
#include<conio.h>
#include<tchar.h>
#include<ctype.h>


struct message {
	char Opcode[1];
	u_short length;
	char Payload[1024];
};

bool check_number(char *buff) {
	int check = 0;
	for (int i = 0; i < strlen(buff); i++) {
		if (!isdigit(buff[i])) {
			check = 1;
			break;
		}
	}
	if (check == 0) return true;
	else return false;
}

char **cut_string(char *buff, const char* seps) {
	int i = 0;
	char **a = (char **)malloc(100 * sizeof(char));
	char *p;
	p = strtok(buff, seps);
	while (p != NULL) {
		a[i] = p;
		p = strtok(NULL, seps);
		i++;
	}
	a[i] = '\0';
	return a;
}

bool check_ip(char *ip) {
	char b[100];
	strcpy(b, ip);
	char **a = cut_string(b, ".");
	if (a[3] != NULL&&a[4] == NULL) {
		int check = 0;
		for (int i = 0; i < 4; i++) {
			if (strlen(a[i]) <= 3 && check_number(a[i]) && atoi(a[i]) >= 0 && atoi(a[i]) <= 255) {
				check++;
			}
		}
		if (check == 4) return true;
		else return false;
	}
	else return false;
}

char *get_filename(char *url_file) {
	char **a = cut_string(url_file, "\\/");
	int i = 0;
	char *b;
	while (a[i] != NULL) {
		b = a[i];
		i++;
	}
	b[strlen(b)] = '\0';
	return b;
}

int getfilesize(char *url) {
	FILE *f = fopen(url, "r");
	if (f == NULL) return 0;
	else {
		//get file size
		fseek(f, 0, SEEK_END);
		int Size = ftell(f);
		fseek(f, 0, SEEK_SET);
		fclose(f);
		return Size;
	}
}

bool check_file(char *url) {
	FILE *f = fopen(url, "r");
	if (f == NULL)
	{
		return false;
	}
	else {
		fclose(f);
		return true;
	}
}

