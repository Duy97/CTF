#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <resolv.h>
#include <netdb.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <fcntl.h> // for open
#include <unistd.h> // for close

#define FAIL -1
// tao ssl socket va dia chi socket
int OpenListener(int port){
	int sd;
	struct sockaddr_in addr;
	sd = socket(PF_INET, SOCK_STREAM,0);
	bzero(&addr, sizeof(addr));
	addr.sin_family =AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;
	if(bind(sd,(struct sockaddr*) &addr,sizeof(addr)) !=0){
		perror("can't bind port");
		abort();
		}
	if (listen(sd,10) !=0){
		perror("khong the cau hinh port");
		abort();
		}
		return sd;
		}
int isRoot(){
	if (getuid() !=0){
		return 0;}
	else {
		return 1;}
	}
SSL_CTX* InitServerCTX(void){
	SSL_METHOD *method;
	SSL_CTX *ctx;
	OpenSSL_add_all_algorithms();
	SSL_load_error_strings();
	method = TLSv1_2_server_method();
	ctx = SSL_CTX_new(method);
	if (ctx == NULL){
		ERR_print_errors_fp(stderr);
		abort();
		}
		return ctx;
		}
void LoadCertificates(SSL_CTX* ctx,char* CertFile, char* KeyFile){
	// thiet lap chung chi tu CertFile
	if (SSL_CTX_use_certificate_file(ctx,CertFile,SSL_FILETYPE_PEM) <=0){
		ERR_print_errors_fp(stderr);
		abort();
		}
	// khoá bi mat
	if (!SSL_CTX_check_private_key(ctx)){
		fprintf(stderr, "%khoa bi mat khong khop voi chung chi cong khai\n", );
		abort();
		}
id ShowCerts(SSL* ssl){
	X509* cert;
	char* line;
	cert = SSL_get_peer_certificate(ssl); // get certificate(if available)
	if (cert != NULL){
		printf(" vching chi server:\n");
		line = X509_NAME_oneline(X509_get_subject_name(cert),0,0);
		printf("subject: %s\n",line );
		free(line);
		line = X509_NAME_oneline(X509_get_issuer_name(cert),0,0);
		printf("issuer: %s\n", line );
		free(line);
		X509_free(cert);
		}
		else{
			printf("khong co chung chi");}
		}
void Servlet(SSL* ssl) {
	char buf[1024] = {0};
	int sd,bytes;
	const char* ServerRespones = "<\Body>\<Name>aticleword.com</Name>\
	<year>1,5</year>\<\Body>";
	const char *cpValidMessage = "<Body>\<UserName>duypham<UserName>\
	<Password>123<Password>\<\Body>";
	if (SSL_accept(ssl)==FAIL){
		ERR_print_errors_fp(stderr);
		else{
			ShowCerts(ssl);
			bytes = SSL_read(ssl,buf,sizeof(buf));//nhan request
			buf[bytes]='\0';
			printf("client massage: %s\n",buf);
			if (bytes >0){
				if (strcmp(cpValidMessage,buf)==0){
					SSL_write(ssl,ServerRespones,strlen(ServerRespones));}
				else {
					SSL_write(ssl,"invalid message", strlen("invalid message");
						}}
			else{
				ERR_print_errors_fp(stderr);}
				}
			sd = SSL_get_fd(ssl); //get socket connetcion
			SSL_free(ssl);
			close(sd);
			}
int main(int count, char* argc[]){
	SSL_CTX * ctx;
	int server;
	char *portnum;
	// chi  co nguoi su quan tri moi dươc chay server
	if(!isRoot()){
		printf("chuong trinh bat buoc phai chay nh la root\n");
		exit(0);}
	if (count !=2){
		printf("usage: %s <portnum>\n", Argc[0]);
		exit(0);
		}
	// thiet lap thu vien ssl 
		SSL_library_init();
		portnum = Argc[1];
		ctx = InitServerCTX(); // thiwet lap ssl
		LoadCertificates(ctx,"mycert.pem","mycert.pem");
		server = OpenListener(atoi(portnum));
		while(1){
			struct sockaddr_in addr;
			socklen_t len = sizeof(addr);
			SSL *ssl;
			int client = accept(server,(struct sockaddr*)&addr,&len);
			printf("connection: %s:%d\n",inet_ntoa(addr.sin_addr),ntohs(addr.sin_port));
			ssl = SSL_new(ctx);
			SSL_set_fd(ssl,client);
			servlet(ssl);}
			close(server);
			SSL_CTX_free(ctx);}


































