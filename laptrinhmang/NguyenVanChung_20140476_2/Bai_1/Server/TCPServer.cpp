#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include "mylibrary.h"//import mylibrary
#define SERVER_PORT 5500
#define SERVER_ADDR "127.0.0.1"
#define BUFF_SIZE 2048
#pragma comment (lib,"ws2_32")

int _tmain(int argc, char*argv[]) {
	//Step 1: Initiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData))
		printf("Version is not supported\n");

	//Step 2: Construct socket	
	SOCKET listenSock;
	listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	//Step 3: Bind address to socket
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	//serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);
	//3.1: Process input
	if (argc == 3) {
		if (strcmp(argv[1], "-p") == 0) {
			if (check_number(argv[2])) {
				serverAddr.sin_port = htons((short)atoi(argv[2]));
				if (bind(listenSock, (sockaddr *)&serverAddr, sizeof(sockaddr))) {
					printf("Error! Cannot bind this address.");
					return 0;
				}
				else {
					printf("Server started in [%s:%s]\n",SERVER_ADDR,argv[2]);
				}
			}
			else {
				printf("Wrong Value!");
				return 0;
			}
		}
		else return 0;
	}
	else return 0;

	//Step 4: Listen request from client
	if (listen(listenSock, 10)) {
		printf("Error! Cannot listen.");
		_getch();
		return 0;
	}

	//Step 5: Communicate with client
	sockaddr_in clientAddr;
	char buff[BUFF_SIZE];
	int ret, clientAddrLen = sizeof(clientAddr);

	while (1) {
		SOCKET connSock;

		//accept request
		connSock = accept(listenSock, (sockaddr *)& clientAddr, &clientAddrLen);

		//receive message from client
		ret = recv(connSock, buff, BUFF_SIZE, 0);
		if (ret == SOCKET_ERROR) {
			printf("Error : %", WSAGetLastError());
			break;
		}
		else if (strlen(buff) > 0) {
			buff[ret] = 0;
			printf("Receive from client[%s:%d] %s\n",
				inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port), buff);
			if (check_buff(buff)) {
				send(connSock, number_buff(buff), strlen(number_buff(buff)), 0);
				send(connSock, alpha_buff(buff), strlen(alpha_buff(buff)), 0);
			}
			else {
				//Echo to client
				strcpy(buff, "ERROR!");
				ret = send(connSock, buff, strlen(buff), 0);
				if (ret == SOCKET_ERROR)
					printf("Error: %", WSAGetLastError());
			}
		}
		closesocket(connSock);
	} //end accepting

	  //Step 5: Close socket
	closesocket(listenSock);

	//Step 6: Terminate Winsock
	WSACleanup();
	_getch();
	return 0;
}