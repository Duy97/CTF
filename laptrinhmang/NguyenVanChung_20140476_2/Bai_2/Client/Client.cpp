// TCPClient.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <tchar.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include "mylibrary.h" //import my library
#define SERVER_PORT 5500
#define SERVER_ADDR "127.0.0.1"
#define BUFF_SIZE 2048
#pragma comment (lib,"ws2_32")
int _tmain(int argc, char* argv[]) {
	//Step 1: Inittiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData))
		printf("Version is not supported\n");

	//Step 2: Construct socket	
	SOCKET client;
	client = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	//(optional) Set time-out for receiving
	int tv = 10000; //Time-out interval: 10000ms
	setsockopt(client, SOL_SOCKET, SO_RCVTIMEO, (const char*)(&tv), sizeof(int));

	//Step 3: Specify server address
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	//serverAddr.sin_port = htons(SERVER_PORT);
	//serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

	//3.1: Input IP and Port for server
	if (argc == 5) {
		if (strcmp(argv[1], "-a") == 0 && strcmp(argv[3], "-p") == 0) {
			if (check_ip(argv[2]) && check_number(argv[4])) {
				serverAddr.sin_port = htons((short)atoi(argv[4]));
				serverAddr.sin_addr.s_addr = inet_addr(argv[2]);
			}
			else {
				printf("Wrong Value!");
				return 0;
			}
		}
		else return 0;
	}
	else return 0;
	//Step 4: Request to connect server
	if (connect(client, (sockaddr *)&serverAddr, sizeof(serverAddr))) {
		printf("Error! Cannot connect server [%s:%s]: %d", argv[2], argv[4], WSAGetLastError());
		_getch();
		return 0;
	}
	printf("Connected server [%s:%s]!\n", argv[2], argv[4]);

	//Step 5: Communicate with server
	char url_file[200], buff[BUFF_SIZE];
	int ret;

	//Input URL File
	printf("Input URL File: ");
	gets_s(url_file, 200);

	//check file
	FILE *File;
	unsigned long Size;
	File = fopen(url_file, "rb");
	if (File == NULL) {
		printf("Error while readaing the file\n");
		return 0;
	}
	else {
		//send file name to server
		char *file_name = get_filename(url_file);
		ret = send(client, file_name, strlen(file_name), 0);
		if (ret == SOCKET_ERROR) {
			printf("Error! Cannot send file_name t server.\n");
			return 0;
		}
		else {
			//receive answer from server
			ret = recv(client, buff, BUFF_SIZE, 0);
			if (ret == SOCKET_ERROR) {
				if (WSAGetLastError() == WSAETIMEDOUT)
					printf("Time-out!");
				else printf("Error! Cannot receive answer.");
				return 0;
			}
			else {
				buff[ret] = '\0';
			}
			if (strcmp(buff, "OK") == 0) {//check answer.

				//get file size
				fseek(File, 0, SEEK_END);
				Size = ftell(File);
				fseek(File, 0, SEEK_SET);

				//send file size.
				char cSize[MAX_PATH];
				sprintf(cSize, "%i", Size);

				ret = send(client, cSize, MAX_PATH, 0);
				if (ret == SOCKET_ERROR) {
					printf("Error! Cannot send file_size to server.");
					return 0;
				}

				//send file to server
				int size_check = 0;
				char *buffer;
				//send big file
				if (Size > 999) {
					buffer = (char*)malloc(1000);
					while (size_check < Size) {
						int read = fread_s(buffer, 1000, sizeof(char), 1000, File);
						int sent = send(client, buffer, read, 0);
						size_check += sent;
						printf("Byte sent: %d", size_check);
					}
				}
				//send small file
				else {
					buffer = (char*)malloc(Size);
					fread_s(buffer, Size, sizeof(char), Size, File);
					send(client, buffer, Size, 0);
				}
				fclose(File);
				free(buffer);
				//receive answer from server
				ret = recv(client, buff, BUFF_SIZE, 0);
				if (ret == SOCKET_ERROR) {
					if (WSAGetLastError() == WSAETIMEDOUT)
						printf("Time-out!");
					else printf("Error! Cannot receive answer.");
					return 0;
				}
				else {
					//print answer
					buff[ret] = '\0';
					printf("%s", buff);
				}

			}
			else {
				//file is exits.
				printf("Exitsting file in server!");
				return 0;
			}
		}

	}
	//Step 6: Close socket
	closesocket(client);

	//Step 7: Terminate Winsock
	WSACleanup();

	return 0;
}


