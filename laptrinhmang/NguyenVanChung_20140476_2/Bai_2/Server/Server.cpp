#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include "mylibrary.h"//import mylibrary
#define SERVER_PORT 5500
#define SERVER_ADDR "127.0.0.1"
#define BUFF_SIZE 2048
#pragma comment (lib,"ws2_32")

int _tmain(int argc, char*argv[]) {
	//Step 1: Initiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData))
		printf("Version is not supported\n");

	//Step 2: Construct socket	
	SOCKET listenSock;
	listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	//Step 3: Bind address to socket
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	//serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);
	//3.1: Process input
	if (argc == 3) {
		if (strcmp(argv[1], "-p") == 0) {
			if (check_number(argv[2])) {
				serverAddr.sin_port = htons((short)atoi(argv[2]));
				if (bind(listenSock, (sockaddr *)&serverAddr, sizeof(sockaddr))) {
					printf("Error! Cannot bind this address.");
					return 0;
				}
				else {
					printf("Server started in [%s:%s]\n", SERVER_ADDR, argv[2]);
				}
			}
			else {
				printf("Wrong Value!");
				return 0;
			}
		}
		else return 0;
	}
	else return 0;

	//Step 4: Listen request from client
	if (listen(listenSock, 10)) {
		printf("Error! Cannot listen.\n");
		_getch();
		return 0;
	}

	//Step 5: Communicate with client
	sockaddr_in clientAddr;
	int ret, clientAddrLen = sizeof(clientAddr);
	FILE *fl;
	while (1) {
		SOCKET connSock;

		//accept request
		connSock = accept(listenSock, (sockaddr *)& clientAddr, &clientAddrLen);

		//receive file name from client
		char file_name[BUFF_SIZE], url_file[] = "../File_Receive/";
		ret = recv(connSock, file_name, BUFF_SIZE, 0);
		if (ret == SOCKET_ERROR) {
			printf("Error! Can not receive file name from client.\n");
			continue;
		}
		else {
			//check file name
			file_name[ret] = '\0';
			printf("File Name: %s\n", file_name);
			strcat(url_file, file_name);
			fl = fopen(url_file, "r");
			if (fl == NULL) {
				//Send answer to client
				ret = send(connSock, "OK", 2, 0);
				if (ret == SOCKET_ERROR) {
					printf("Error! Can not send answer to client.\n");
					continue;
				}

				//receive file_size from client
				int Size;
				char *Filesize = new char[1024];
				if (recv(connSock, Filesize, 1024, 0)) // File size
				{
					Size = atoi((const char*)Filesize);
					printf("File size: %d byte\n", Size);
				}

				//receive file from client
				FILE *File = fopen(url_file, "wb");
				char *buffer;
				int size_check = 0;
				//receive big file.
				if (Size > 999) {
					buffer = (char*)malloc(1000);
					while (size_check < Size) {
						int rc = recv(connSock, buffer, 1000, 0);
						size_check += rc;
						fwrite(buffer, 1, rc, File);
						fflush(File);
					}
					//give the results to the client
					if (size_check = Size) {
						printf("Save file complete!\n");
						ret = send(connSock, "Send file Complete!", 19, 0);//Successful
						if (ret == SOCKET_ERROR) {
							printf("Error! Can not send result to client.\n");
							continue;
						}
					}
					else {
						printf("Save not file complete!\n");
						ret = send(connSock, "Send file Not Complete!", 23, 0);//not successful
						if (ret == SOCKET_ERROR) {
							printf("Error! Can not send result to client.\n");
							continue;
						}
					}
				}
				//receive small file
				else {
					buffer = (char*)malloc(Size);
					int rc = recv(connSock, buffer, Size, 0);
					fwrite(buffer, 1, rc, File);
					fflush(File);
					printf("Save file complete!\n");
					ret = send(connSock, "Send file Complete!", 19, 0);
					if (ret == SOCKET_ERROR) {
						printf("Error! Can not send result to client.\n");
						continue;
					}
				}
				fclose(File);
				free(buffer);

			}
			else {
				fclose(fl);
				//give the results to the client
				ret = send(connSock, "NO", 2, 0);
				if (ret == SOCKET_ERROR) {
					printf("Error! Can not send answer to client.\n");
					continue;
				}
			}
			closesocket(connSock);
		} //end accepting
	}
	//Step 5: Close socket
	closesocket(listenSock);
	
	//Step 6: Terminate Winsock
	WSACleanup();
	return 0;
}