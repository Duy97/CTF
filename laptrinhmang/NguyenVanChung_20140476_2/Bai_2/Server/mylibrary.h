#pragma once
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <ctype.h>
#include <stdlib.h>
bool check_buff(char *buff) {
	if (strlen(buff) > 0) {
		int j = 0;
		for (int i = 0; i < (int)strlen(buff); i++) {
			if (isalpha(buff[i]) || isdigit(buff[i])) j++;
			else break;
		}
		if (j == (int)strlen(buff)) return true;
		else return false;
	}
	else return true;
}

char *number_buff(char *buff) {
	char a[2048];
	char b[2048] = "";
	if (check_buff(buff)) {
		for (int i = 0; i < (int)strlen(buff); i++) {
			strcpy(a, &buff[i]);
			a[1] = '\0';
			if (isdigit(buff[i])) {
				strcat(b, a);
			}
		}
		return b;
	}
	else {
		return NULL;
	}
}

char *alpha_buff(char *buff) {
	char a[2048];
	char b[2048] = "";
	if (check_buff(buff)) {
		for (int i = 0; i < (int)strlen(buff); i++) {
			strcpy(a, &buff[i]);
			a[1] = '\0';
			if (isalpha(buff[i])) {
				strcat(b, a);
			}
		}
		return b;
	}
	else {
		return NULL;
	}
}
char **cut_string(char *buff, const char* seps) {
	int i = 0;
	char **a = (char **)malloc(100 * sizeof(char));
	char *p;
	p = strtok(buff, seps);
	while (p != NULL) {
		a[i] = p;
		p = strtok(NULL, seps);
		i++;
	}
	a[i] = '\0';
	return a;
}
bool check_number(char *Char) {
	int c = 0;//check value
	for (int i = 0; i < strlen(Char); i++) {
		if (!isdigit(Char[i])) c = 1;
	}
	if (c == 1) return false;
	else return true;
}

bool check_ip(char *ip) {
	char b[100];
	strcpy(b, ip);
	char **a = cut_string(b, ".");
	if (a[3] != NULL&&a[4] == NULL) {
		int check = 0;
		for (int i = 0; i < 4; i++) {
			if (strlen(a[i]) <= 3 && check_number(a[i]) && atoi(a[i]) >= 0 && atoi(a[i]) <= 255) {
				check++;
			}
		}
		if (check == 4) return true;
		else return false;
	}
	else return false;
}

char *get_filename(char *url_file) {
	char **a = cut_string(url_file, "\\/");
	int i = 0;
	char *b;
	while (a[i] != NULL) {
		b = a[i];
		i++;
	}
	b[strlen(b)] = '\0';
	return b;
}
