#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>


int compare_strings(char a[], char b[])
{
    int c = 0;
    while (a[c] == b[c]) 
    {
        if (a[c] == '\0' || b[c] == '\0')
        break;
        c++;
    }
    if (a[c] == '\0' && b[c] == '\0')
    return 0;
    else
    return -1;
}

char encrypt(char msg[1024]){	
	long int m[1024], len, i, k, ct[1024];
	len = strlen(msg);
	for(i=0;i<len;i++)
	{	m[i] = msg[i];
		k = m[i] - 96;
		k=pow(k,7);
		k = k%35+40;
		ct[i] = k;
		msg[i] = ct[i];
	}

}

char decrypt(char msg[1024]){
	long int m[1024], len, i, k, pt[1024];
	len = strlen(msg);
	for(i=0;i<len;i++)
	{	m[i] = msg[i];
		k = m[i] -40;
		k=pow(k,7);
		k = k%35 + 96;
		pt[i] = k;
		msg[i] = pt[i];
	}

}

int main() {
   
    
    int clientSocket;
    char buffer[1024];
    struct sockaddr_in serverAddr;
    socklen_t addr_size;
    int cmdEXIT = 0;

    
    clientSocket = socket(PF_INET, SOCK_STREAM, 0);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(7891);
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);
    addr_size = sizeof serverAddr;

    
    connect(clientSocket, (struct sockaddr *) &serverAddr, addr_size);

    
    while (cmdEXIT == 0)
    {
        
        int recvValue = recv(clientSocket, buffer, sizeof buffer - 1, 0);
	decrypt(buffer);
       
        if (recvValue != 1)
        {
                  
            if (compare_strings(buffer, "exit")==-1)
            {
               
                printf("Client 1 : ");
                printf("%s\n", buffer);
                memset(&buffer[0], 0, sizeof(buffer));

            }
           
            else cmdEXIT = 1;
        }
        else
        {
            printf("Client 2 : ");
            scanf(" %[^\n]s", buffer);
		encrypt(buffer);
           
            send(clientSocket,buffer,sizeof buffer - 1,0);
           
            if (compare_strings(buffer, "exit")==-1)
            {
              
                memset(&buffer[0], 0, sizeof(buffer));
            }
          
            else cmdEXIT = 1;
        }   
    }
    return 0;
}
