#pragma once
#include <iostream>
using namespace std;
class CheckError
{
public:
	CheckError(){
	}
	/* So ky tu toi da tren 1 dong */
	int InputMaxCharacterInALine(){
		int error;
		int maxCharacterInALine = 30;
		cout << "---Yeu cau nhap 1 so nguyen, chuong trinh se chay tiep khi ban nhap dung---" << endl;
		do{
			error = 0;
			cout << "Nhap so ki tu toi da tren 1 dong: " << endl;
			cin >> maxCharacterInALine;
			if (cin.fail()){
				cout << "Ban nhap gia tri khong dung!" << endl;
				error = 1;
				cin.clear();
				cin.ignore(80, '\n');
			}
		} 
		while (error == 1);
		return maxCharacterInALine;
	}
	
	char InputCharacterAsSpace(){  /* dau ra khong chua khoang trang */
		char characterUsedAsSpace = ' ';
		string input = "";
		char c;
		int error;
		cout << "Mac dinh ky tu ngan cach la space, ban co muon thay doi khong? c/k";
		cin >> c;
		if (cin.fail()){
			cin.clear();
			cin.ignore(80, '\n');
			cout << "Ki tu ban nhap khong dung, chuong trinh se su dung space" << endl;
			return characterUsedAsSpace;
		}
		else{
			if (c != 'c')
			{
				if (c != 'k')
				{
					cin.clear();
					cin.ignore(80, '\n');
					cout << "Ban khong chon c/k, chuong trinh se dung ky tu mac dinh" << endl;
					return characterUsedAsSpace;
				}
				cin.clear();
				cin.ignore(80, '\n');
				return characterUsedAsSpace;
			}
			
			cout << "---Yeu cau nhap 1 ky tu, chuong trinh se chay tiep khi ban nhap dung---" << endl;
			cin.clear();
			cin.ignore(80, '\n');
			do
			{
				error = 0;
				cout << "Nhap ki tu lam khoang trang: " << endl;
				//cin >> input;
				getline(cin, input);
				if (input.length() != 1)
				{
					cout << "Ban chi duoc phep nhap 1 ky tu!" << endl;
					error = 1;
					/*cin.clear();
					cin.ignore(80, '\n');*/
				}
			} while (error == 1);
			characterUsedAsSpace = input.front();
			return characterUsedAsSpace;
		}
	}

	bool CheckIfFileExist(string fileName)	{ // kiem tra file dau ra
		if (std::ifstream(fileName))
		{
			return true;
		}
		return false;
	}

	string CheckInputFile() { // kiem tra file dau vao
		string fileName = "";
		int error;
		do{
			error=0;
			cout << "Nhap ten file text input (nhap input.txt)" << endl;
			getline(cin, fileName);
			if (!CheckIfFileExist(fileName)){
				cout<<"File khong ton tai" <<endl;
				error=1;
			}
		}while(error==1);
		return fileName;	
	}
};

