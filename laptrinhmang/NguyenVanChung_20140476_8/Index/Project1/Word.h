#pragma once
#include <string>

using namespace std;
int const MAX_WORD_SIZE = 20;
class Word
{
private:
	string word;
public:
	Word *next;
	Word()
	{
		word = "";
		next = NULL;
	}
	Word(string word)
	{
		this->word=word;
		next = NULL;
	}
	void SetWord(string word)
	{
		this->word= word;
	}
	string GetWord()
	{
		return word;
	}
	int GetWordLength()
	{
		return word.length();
	}
	Word* ReadWord()
	{
		char word[MAX_WORD_SIZE];
		int ch, pos;
		pos = 0;
		ch = getchar();

		while (isspace(ch))
		{
			ch = getchar();
		}
		while (!isspace(ch) && (ch != EOF))
		{
			if (pos<MAX_WORD_SIZE)
			{
				word[pos] = (char)ch;
				pos++;
			}
			ch = getchar();
		}
		word[pos] = '\0';
		string str = word;
		return new Word(str);
	}
	
};

