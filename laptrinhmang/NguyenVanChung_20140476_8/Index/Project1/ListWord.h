#pragma once
#include <string>
#include "Word.h"
#include <iostream>

using namespace std;
class ListWord
{
private:
	Word *head;
	int maxNumber;
	char character;
public:
	ListWord(){
		head = NULL;
		maxNumber = 30;
		character = ' ';
	}

	void setmaxNumber(int maxNumber){
		this->maxNumber= maxNumber;
	}

	void setCharacter(char character){
		this->character = character;
	}

	void AddWordToList(Word *word){ // them van ban trong 1 danh sach
		if(head==NULL){
			word->next = head;
			head = word;
		}
		else{
			Word *temp = head;
			while(temp->next!=NULL){
				temp=temp->next;
			}
			temp->next = word;
		}
	}

	void PrintListWord(){   // in danh sach van ban 
		Word *temp = head;
		while(temp!=NULL){
			cout<<temp->GetWord()<<" ";
			temp = temp->next;
		}
	}

	string AddAllWordsToString(){ // them tat ca van ban ve dng string
		Word *temp = head;
		string str="";
		while (temp != NULL){
			//cout << temp->GetWord() << " ";
			str+=temp->GetWord();
			str.push_back(' ');
			temp = temp->next;
		}
		return str;
	}


	string AlignLeft(){        // can le trai
		int totalNumberOfCharacter;
		int i = 0;
		string fileContent = "";
		string previousFileContent = "";
		Word *temp = head;
		while (temp != NULL){
			previousFileContent = fileContent;
			fileContent.append(temp->GetWord());
			totalNumberOfCharacter = fileContent.length();
			fileContent.push_back(character);
			if ((totalNumberOfCharacter-i)>maxNumber){
				fileContent = previousFileContent;
				fileContent.pop_back();
			
				fileContent.push_back('\n');
				i = fileContent.length();
				
				fileContent += temp->GetWord();
				fileContent.push_back(character);
			}
			temp = temp->next;
		}
		return fileContent;
	}

	string AlignRight() { // can le phai
		int totalNumberOfCharacter=0;
		string line="";
		string tempFileContent = "";
		string fileContent="";
		string previousFileContent = "";
		Word *temp = head;
		int i;
		while (temp != NULL){
			previousFileContent = tempFileContent;
			tempFileContent.append(temp->GetWord());
			tempFileContent.push_back(character);
			totalNumberOfCharacter += temp->GetWordLength();
			if ((totalNumberOfCharacter)>maxNumber)
			{
				tempFileContent = previousFileContent;
				tempFileContent.pop_back();
				totalNumberOfCharacter -=temp->GetWordLength()+1;
				tempFileContent.push_back('\n');
				
				if (maxNumber- totalNumberOfCharacter>0){
					for (i = 0; i< maxNumber - totalNumberOfCharacter; i++)
					{
						tempFileContent = " "+tempFileContent;
					}
				}
				fileContent +=tempFileContent;
				tempFileContent="";
				totalNumberOfCharacter=0;
				tempFileContent += temp->GetWord();
				tempFileContent.push_back(character);
				totalNumberOfCharacter += temp->GetWordLength();	
			}
			previousFileContent = tempFileContent;
			totalNumberOfCharacter +=1;
			temp = temp->next;
		}
		int l = previousFileContent.length()-1;
		int j;
		for(j=0;j<maxNumber-l;j++){
			previousFileContent = " "+previousFileContent;
		}
		fileContent=fileContent+previousFileContent;
		fileContent.pop_back();
		return fileContent;
	}
};

