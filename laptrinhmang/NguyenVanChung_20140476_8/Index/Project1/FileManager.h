#pragma once
#include <fstream>
#include <string>
#include <iostream>
using namespace std;
class FileManager
{
public:
	FileManager(){	
	}
	
	string ConvertTextFileToString(string fileName = "input.txt"){ // chuyen van ban sang kieu string 
		ifstream file(fileName);
		string line;
		string fileContent;
		while (getline(file, line)){
			fileContent += line;
			fileContent.push_back('\n');
		}
		file.close();
		return fileContent;
	}

	void BackUp(ListWord *list){  
		list->setmaxNumber(30);
		list->setCharacter(' ');
		string backUp = list->AlignLeft();
		SaveToFile(backUp, "backup.txt");
	}

	bool CheckIfFileExist(string fileName)	{  //kiem tra FileExit
		if (std::ifstream(fileName))
		{
			return true;
		}
		return false;
	}

	bool SaveToFile(string fileContent, string destinationFileName = "output.txt")  //Luu file
	{
		ofstream myfile(destinationFileName);
		if (myfile.is_open())
		{
			myfile << fileContent;
			myfile.close();
			return true;
		}
		else return false;
	}
	ListWord *ConvertStringToListOfWords(string stringContent, ListWord* list) //chuyen dang String qua dang Word
	{
		stringstream stream(stringContent);
		string word;
		while (stream >> word)
		{
			Word *word2 = new Word(word);
			list->AddWordToList(word2);
		}
		return list;
	}
};

