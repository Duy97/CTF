#pragma once
#include "ListWord.h"
#include "FileManager.h"
#include "CheckError.h"

using namespace std;

class UI
{
private:
	ListWord *list;
	FileManager *fileManager;
	Word *word;
	CheckError *checkError;
public:
	UI()
	{
		fileManager = new FileManager();
		list = new ListWord();
		word = new Word();
		checkError = new CheckError();
	}

	void RunProgram()
	{
		//So ki tu toi da tren 1 dong.
		list->setmaxNumber(checkError->InputMaxCharacterInALine());

		//Ki tu dung lam khoang trang
		list->setCharacter(checkError->InputCharacterAsSpace());

		//Ten file txt dau ra
		string outputFile = "output.txt";



		//Chuyen noi dung file vao 1 chuoi string
		string fileContent = fileManager->ConvertTextFileToString(checkError->CheckInputFile());

		//Chuyen toan bo chuoi string thanh 1 list cac word
		list = fileManager->ConvertStringToListOfWords(fileContent, list);

		//Back up lai file
		/*fileManager->BackUp(list);
		list->setmaxNumber(30);
		list->setCharacter(' ');*/
		
		cout << "Toan bo file text da duoc load vao trong chuong trinh, ban co muon them tu khong? c/k " << endl;
		char c = ' ';
		cin >> c;

		if (c == 'c')
		{
			InputFromKeyboard();
		}
		cin.clear();

		char d = ' ';
		cout << "Ban muon can le trai hay phai? t/p " << endl;
		cin >> d;
		
		if (d == 't')
		{
			//Can le trai
			string contentAlignLeft = list->AlignLeft();
			cout << contentAlignLeft << endl;
			fileManager->SaveToFile(contentAlignLeft, "output.txt");
			cout << "Check file output.txt de xem ket qua!" << endl;
		}
		else
		{
			//Can le phai
			string contentAlignRight = list->AlignRight();
			cout << contentAlignRight << endl;
			fileManager->SaveToFile(contentAlignRight, "output.txt");
			cout << "Check file output.txt de xem ket qua!" << endl;
		}
		cin.clear();
	}





	void InputFromKeyboard()
	{

		char c = 'c';
		while (c != 'k')
		{
			cout << "Nhap tu vao van ban (Khong de khoang trang): " << endl;
			Word *tempword = word->ReadWord();
			list->AddWordToList(tempword);
			cout << "Ban co muon tiep tuc hay khong ? c/k " << endl;
			cin >> c;
			cin.clear();
		}
	}
};

