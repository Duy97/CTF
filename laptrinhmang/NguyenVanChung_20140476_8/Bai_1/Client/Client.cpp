#include<stdio.h>
#include<WinSock2.h>
#include<WS2tcpip.h>
#include<tchar.h>
#include "mylibrary.h"


#pragma comment (lib,"Ws2_32")


//functions is used in the main function.
void slt_protocol(SOCKET client);
void Login(SOCKET client);
void Sendpass(SOCKET client);
void Logout(SOCKET client);



//main function
int _tmain(int argc, char *argv[]) {

	//Step 1: Initiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData)) {
		printf("Version is not support!\n");
		return 0;
	}
	//Step 2: Contruct socket
	SOCKET client;
	client = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	//(optional) Set time-out for receiving
	int tv = 15000; //Time-out interval: 10000ms
	setsockopt(client, SOL_SOCKET, SO_RCVTIMEO, (const char*)(&tv), sizeof(int));


	//Step 3: Specify server address
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;

	//Process command line parameters
	if (argc == 5) {
		if (strcmp(argv[1], "-a") == 0 && strcmp(argv[3], "-p") == 0) {
			if (check_number(argv[4]) && check_ip(argv[2])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[4]));
				serverAddr.sin_addr.s_addr = inet_addr(argv[2]);
			}
			else {
				printf("ERROR : Wrong Value!");
				return 0;
			}
		}
		else {
			printf("ERROR : Command line parameter is wrong!");
			return 0;
		}
	}
	else {
		printf("ERROR : Command line parameter is wrong!");
		return 0;
	}

	//Step 4: Request to connect server
	if (connect(client, (sockaddr *)&serverAddr, sizeof(serverAddr))) {
		printf("Error! Cannot connect server [%s:%s]: %d", argv[2], argv[4], WSAGetLastError());
		_getch();
		return 0;
	}
	printf("Connected server in address [%s:%s]!\n", argv[2], argv[4]);


	//Step 5: Communicate with server
	slt_protocol(client);

	//Close Socket
	closesocket(client);

	//Terminate WinSock
	WSACleanup();

}


//function select protocol LOGIN or LOGOUT
void slt_protocol(SOCKET client) {
	char protocol[100];
	int check = 0;
	do {
		printf("Select Protocol [LOGIN or LOGOUT OR EXIT]: ");
		gets_s(protocol, 100);
		if (strcmp(protocol, "LOGIN") == 0) {
			check = 1;
			Login(client);
			return;
		}
		else {
			if (strcmp(protocol, "LOGOUT") == 0) {
				check = 1;
				Logout(client);
				return;
			}
			else {
				if (strcmp(protocol, "EXIT") == 0) {
					return;
				}
				else {
					printf("Wrong!!!\n");
					printf("please Try Again!\n");
				}
			}
		}
	} while (check == 0);
}




//function send id for server
void Login(SOCKET client) {
	message MES;
	int ret;
	char *buff = new char[2048];
	char id[100];
	printf("LOGIN\n");
	printf("USER:");
	gets_s(id, 100);
	MES.TYPE = 1;
	memcpy(MES.mes, id, strlen(id) + 1);
	ret = send(client, (char*)&MES, sizeof(MES), 0);
	if (ret == SOCKET_ERROR) {
		printf("Error! Cannot send message.\n");
		return;
	}
	ret = recv(client, buff, 2048, 0);
	if (ret == SOCKET_ERROR) {
		if (WSAGetLastError() == WSAETIMEDOUT)
			printf("Time-out!\n");
		else printf("Error! Cannot receive answer.\n");
		Login(client);
		//return;
	}
	else {
		buff[ret] = '\0';
		int check = atoi(buff);
		switch (check) {
		case 01:
		{
			printf("Specified User!\n");
			Sendpass(client);
			break;
		}
		case 11:
		{
			printf("Can not found this id!\n");
			Login(client);
			break;
		}
		case 21:
		{
			printf("You are logged!\n");
			printf("If you want to login with other account you must logout current account!\n\n");
			slt_protocol(client);
			break;
		}
		default:
			break;

		}
	}

}


//function send pass for server
void Sendpass(SOCKET client) {
	message mess;
	int ret;
	char *buff = new char[2048];
	char psw[100];
	printf("PASS:");
	gets_s(psw, 100);
	mess.TYPE = 2;
	memcpy(mess.mes, psw, strlen(psw) + 1);
	ret = send(client, (char*)&mess, sizeof(mess), 0);
	if (ret == SOCKET_ERROR) {
		printf("Error! Cannot send message.\n");
		return;
	}
	ret = recv(client, buff, 2048, 0);
	if (ret == SOCKET_ERROR) {
		if (WSAGetLastError() == WSAETIMEDOUT)
			printf("Time-out!\n");
		else printf("Error! Cannot receive answer.\n");
		Sendpass(client);
		//return;
	}
	else {
		buff[ret] = '\0';
		int check = atoi(buff);
		switch (check) {
		case 02:
		{
			printf("Authenticated!\n\n\n");
			printf("If you want to logout, you can select protocol LOGOUT.\n");
			printf("OR if you want to login with other user you can LOGOUT current user and select protocol LOGIN.\n\n");
			slt_protocol(client);
			break;
		}
		case 12:
		{
			printf("Password is'n incorrect!\n");
			Sendpass(client);
			break;
		}
		case 22:
		{
			printf("Did'n specify user!\n\n");
			slt_protocol(client);
			break;
		}
		default:
			break;

		}
	}
}



//function logout
void Logout(SOCKET client) {
	message mess;
	int ret;
	char *buff = new char[2048];
	mess.TYPE = 3;
	ret = send(client, (char*)&mess, sizeof(mess), 0);
	if (ret == SOCKET_ERROR) {
		printf("Error! Cannot send message.\n");
		return;
	}
	ret = recv(client, buff, 2048, 0);
	if (ret == SOCKET_ERROR) {
		if (WSAGetLastError() == WSAETIMEDOUT)
			printf("Time-out!\n");
		else printf("Error! Cannot receive answer.\n");
		slt_protocol(client);
		//return;
	}
	else {
		buff[ret] = '\0';
		int check = atoi(buff);
		switch (check) {
		case 03:
		{
			printf("Un-Authenticated!\n\n\n");
			printf("If you want to login with other account you can select LOGIN!\n\n");
			slt_protocol(client);
			break;
		}
		case 13:
		{
			printf("You did'n login, please try again!\n\n");
			slt_protocol(client);
			break;
		}
		default:
			break;

		}
	}
}