// Server.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <conio.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <tchar.h>
#include "mylibrary.h"

#define SERVER_ADDR "127.0.0.1"
#define DATA_BUFSIZE 1024
#pragma comment (lib,"ws2_32")


account *create_acc(int n) {
	account *acc = new account[n];
	for (int i = 0; i < n; i++) {
		account new_acc;
		strcpy(new_acc.id, "chungnv");
		strcpy(new_acc.psw, "password");
		char c[100];
		sprintf(c, "%i", i);
		strcat(new_acc.id, c);
		strcat(new_acc.psw, c);
		new_acc.failed_login = 0;
		acc[i] = new_acc;
	}
	return acc;
}

void prc_mess(state_client *, char *, account *);
int receiveData(SOCKET, char *, int, int);
int sendData(SOCKET, char *, int, int);

account *acc = create_acc(10);
SOCKET connSock;

void CALLBACK WorkerRoutine(DWORD Error, DWORD BytesTransferred, LPWSAOVERLAPPED Overlapped, DWORD InFlags);
DWORD WINAPI WorkerThread(LPVOID lpParameter);


int _tmain(int argc, char *argv[]) {

	DWORD flags, recvBytes, index;
	int ret;
	WSAEVENT events[1];
	WSAOVERLAPPED overlapped;
	HANDLE ThreadHandle;
	DWORD ThreadId;

	//Step 1: Initiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData)) {
		printf("Version is not supported!\n");
	}

	//Step 2: Construct socket
	SOCKET listenSock;
	if ((listenSock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED)) == INVALID_SOCKET)
	{
		printf("Failed to get a socket %d\n", WSAGetLastError());
		return 1;
	}
	else
		printf("WSASocket() is pretty fine!\n");

	// Associate event types FD_ACCEPT and FD_CLOSE
	// with the listening socket and newEvent

	//Step 3: Bind address to socket
	sockaddr_in serverAddr, clientAddr;
	int clientAddrlen = sizeof(clientAddr);
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

	if (argc == 3) {
		if (strcmp(argv[1], "-p") == 0) {
			if (check_number(argv[2])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[2]));
				//Bind
				if (bind(listenSock, (sockaddr*)&serverAddr, sizeof(sockaddr))) {
					printf("ERROR: Can not bind this address!");
					return 0;
				}
				printf("Server started in [%s:%s]\n", SERVER_ADDR, argv[2]);
			}
			else {
				printf("ERROR : Command line parameter is wrong!");
				return 0;
			}
		}
		else {
			printf("ERROR : Command line parameter is wrong!");
			return 0;
		}
	}
	else {
		printf("ERROR : Command line parameter is wrong!");
		return 0;
	}

	//Step 4: Listen request from client
	if (listen(listenSock, 10)) {
		printf("ERROR! Can not listen!");
		_getch();
		return 0;
	}

	if ((events[0] = WSACreateEvent()) == WSA_INVALID_EVENT)
	{
		printf("WSACreateEvent() failed with error %d\n", WSAGetLastError());
		return 1;
	}
	else
		printf("WSACreateEvent() is OK!\n");

	// Create a worker thread to service completed I/O requests
	if ((ThreadHandle = CreateThread(NULL, 0, WorkerThread, (LPVOID)events[0], 0, &ThreadId)) == NULL)
	{
		printf("CreateThread() failed with error %d\n", GetLastError());
		return 1;
	}
	else
		printf("CreateThread() should be fine!\n");


	while (TRUE)
	{
		connSock = accept(listenSock, (sockaddr*)&clientAddr, &clientAddrlen);

		if (WSASetEvent(events[0]) == FALSE)
		{
			printf("WSASetEvent() failed with error %d\n", WSAGetLastError());
			return 1;
		}
		else
			printf("WSASetEvent() should be working!\n");
	}
}

//function thread
DWORD WINAPI WorkerThread(LPVOID lpParameter){
	DWORD Flags;
	state_client* SocketInfo = new state_client[1];
	WSAEVENT events[1];
	DWORD Index;

	// Save the accept event in the event array
	events[0] = (WSAEVENT)lpParameter;

	while (TRUE)
	{
		// Wait for accept() to signal an event and also process WorkerRoutine() returns
		while (TRUE)
		{
			Index = WSAWaitForMultipleEvents(1, events, FALSE, WSA_INFINITE, TRUE);
			if (Index == WSA_WAIT_FAILED)
			{
				printf("WSAWaitForMultipleEvents() failed with error %d\n", WSAGetLastError());
				return FALSE;;
			}
			else
				printf("WSAWaitForMultipleEvents() should be OK!\n");

			if (Index != WAIT_IO_COMPLETION)
			{
				// An accept() call event is ready - break the wait loop
				break;
			}
		}
		WSAResetEvent(events[Index - WSA_WAIT_EVENT_0]);
		// Create a socket information structure to associate with the accepted socket
		if ((SocketInfo = (state_client*)GlobalAlloc(GPTR, sizeof(state_client))) == NULL)
		{
			printf("GlobalAlloc() failed with error %d\n", GetLastError());
			return FALSE;
		}
		else
			printf("GlobalAlloc() for SOCKET_INFORMATION is OK!\n");

		// Fill in the details of our accepted socket
		SocketInfo[0].connSock = connSock;
		ZeroMemory(&(SocketInfo[0].Overlapped), sizeof(WSAOVERLAPPED));
		SocketInfo[0].BytesRECV = 0;
		SocketInfo[0].DataBuf.len = DATA_BUFSIZE;
		SocketInfo[0].DataBuf.buf = new char[DATA_BUFSIZE];
		SocketInfo[0].state = "Unauthenticated";

		Flags = 0;
		if (WSARecv(SocketInfo[0].connSock, &(SocketInfo[0].DataBuf), 1, &SocketInfo[0].BytesRECV, &Flags,
			&(SocketInfo[0].Overlapped), WorkerRoutine) == SOCKET_ERROR)
		{
			if (WSAGetLastError() != WSA_IO_PENDING)
			{
				printf("WSARecv() failed with error %d\n", WSAGetLastError());
				return FALSE;
			}
		}
		else
			printf("WSARecv() is OK!\n");

		printf("Socket %d got connected...\n", connSock);
	}
}

//Callback function
void CALLBACK WorkerRoutine(DWORD error, DWORD bytesTransferred, LPWSAOVERLAPPED lpOverlapped, DWORD inFlags) {

	DWORD recvBytes;
	DWORD Flags;
	state_client st_cl;
	if (error != 0 || bytesTransferred == 0) {

		// Either a bad error occurred on the socket or the socket was closed by a peer
		closesocket(connSock);
		return;
	}
	//Process revData
	memcpy(&st_cl, lpOverlapped, sizeof(state_client));
	st_cl.BytesRECV = bytesTransferred;
	st_cl.DataBuf.buf[bytesTransferred] = 0;
	printf("\nReceive: %s", st_cl.DataBuf.buf);
	//process
	prc_mess(&st_cl, st_cl.DataBuf.buf,acc);
	
	// Step 8: Call I/O function again ...
	Flags = 0;
	ZeroMemory(&(st_cl.Overlapped), sizeof(WSAOVERLAPPED));
	st_cl.DataBuf.len = DATA_BUFSIZE;
	st_cl.DataBuf.buf = new char[DATA_BUFSIZE];
	if (WSARecv(st_cl.connSock, &st_cl.DataBuf, 1, &st_cl.BytesRECV, &Flags, &(st_cl.Overlapped), WorkerRoutine) == SOCKET_ERROR)
	{
		if (WSAGetLastError() != WSA_IO_PENDING)
		{
			printf("WSARecv() failed with error %d\n", WSAGetLastError());
			return;
		}
	}
	else
		printf("WSARecv() is fine!\n");


}

/*The process message function: process message and send answer to client*/
void prc_mess(state_client *state_cl, char *buff, account *acc) {
	state_client st_cl;
	memcpy(&st_cl, state_cl, sizeof(state_client));
	message mes;
	memcpy(&mes, buff, sizeof(message));
	switch (mes.TYPE)
	{
	case 1: { //Specify user
		if (strcmp(st_cl.state, "Un-authenticated") == 0) {
			int check = check_acc(acc, mes.mes, 10);
			if (check == 0) {//not found id
				sendData(st_cl.connSock, "11", 2, 0);
			}
			else {//found id
				sendData(st_cl.connSock, "01", 2, 0);
				st_cl.state = "Specified-user";
				memcpy(&st_cl.acc, &acc[check - 1], sizeof(account));
			}
		}
		else {
			sendData(st_cl.connSock, "21", 2, 0);
		}
		break;
	}
	case 2: {	//Mathed password
		if (strcmp(st_cl.state, "Specified-user") == 0) {
			if (check_pass(mes.mes, st_cl.acc)) {
				sendData(st_cl.connSock, "02", 2, 0);
				st_cl.state = "Authenticated";
			}
			else {
				sendData(st_cl.connSock, "12", 2, 0);
			}
		}
		else {
			sendData(st_cl.connSock, "22", 2, 0);

		}
		break;
	}
	case 3: {	//LOGOUT
		if (strcmp(st_cl.state, "Authenticated") == 0) {
			sendData(st_cl.connSock, "03", 2, 0);
			st_cl.state = "Un-authenticated";
		}
		else {
			sendData(st_cl.connSock, "13", 2, 0);
		}
		break;
	}
	default:
		break;
	}
	memcpy(state_cl, &st_cl, sizeof(state_client));
}

/*The recv() wrapper function*/
int receiveData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = recv(s, buff, size, flags);
	if (n <= 0) {
		//printf("\nOne client disconnected.");
	}
	else {
		buff[n] = '\0';
		//printf("\nreceive one message!");
	}
	return n;
}


/*The send() wrapper function*/
int sendData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = send(s, buff, size, flags);
	//if (n <= 0) printf("\nsend error.");
	return n;
}