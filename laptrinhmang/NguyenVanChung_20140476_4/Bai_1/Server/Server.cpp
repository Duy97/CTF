#include<stdio.h>
#include<WinSock2.h>
#include<WS2tcpip.h>
#include<tchar.h>

#include "mylibrary.h"
#include <process.h>

#define SERVER_ADDR "127.0.0.1"
#define BUFF_SIZE 2048

#pragma comment (lib,"Ws2_32")

void prc_mess(state_client st_cl,account *acc,sockaddr_in claddr);

unsigned __stdcall log_thread(void *param) {
	data_thread dt_th;
	memcpy(&dt_th, param, sizeof(data_thread));
	prc_mess(dt_th.st_cl, dt_th.acc,dt_th.claddr);
	return 0;
}

account *create_acc(int n) {
	account *acc = new account[n];
	for (int i = 0; i < n; i++) {
		account new_acc;
		strcpy(new_acc.id, "chungnv");
		strcpy(new_acc.psw, "password");
		char c[100];
		sprintf(c, "%i", i);
		strcat(new_acc.id, c);
		strcat(new_acc.psw, c);
		new_acc.failed_login = 0;
		acc[i] = new_acc;
	}
	return acc;
}

int _tmain(int argc, char *argv[]) {

	//Step 1: Initiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData)) {
		printf("Version is not support!\n");
		return 0;
	}
	//Step 2: Contruct socket
	SOCKET listenSock;
	listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	//Step 3: Bind address to socket
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

	//Process command line parameters
	if (argc == 3) {
		if (strcmp(argv[1], "-p") == 0) {
			if (check_number(argv[2])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[2]));
				//Bind
				if (bind(listenSock, (sockaddr*)&serverAddr, sizeof(sockaddr))) {
					printf("ERROR: Can not bind this address!");
					return 0;
				}
				printf("Server started in [%s:%s]\n", SERVER_ADDR, argv[2]);
			}
			else {
				printf("ERROR : Command line parameter is wrong!");
				return 0;
			}
		}
		else {
			printf("ERROR : Command line parameter is wrong!");
			return 0;
		}
	}
	else {
		printf("ERROR : Command line parameter is wrong!");
		return 0;
	}

	//Step 4: Listen request from client
	if (listen(listenSock, 10)) {
		printf("Error! Cannot listen.");
		_getch();
		return 0;
	}

	//Step 5: Communicate with client
	
	//create id and password
	//account *acc = create_acc(10);
	//variable
	sockaddr_in clientAddr;
	char buff[BUFF_SIZE];
	int ret, clientAddrLen = sizeof(clientAddr);
	while (1) {

		state_client st_cl;
		data_thread dt_th;
		
		//accept request
		st_cl.connSock = accept(listenSock, (sockaddr *)& clientAddr, &clientAddrLen);
		st_cl.failed_login = 0;
		st_cl.state = "Un-authenticated";
		dt_th.st_cl = st_cl;
		dt_th.acc = create_acc(10);
		dt_th.claddr = clientAddr;
		//memcpy(dt_th.acc, acc, 10 * sizeof(account));
		
		//thread function
		_beginthreadex(0, 0, log_thread, (void*)&dt_th, 0, 0);

		
		//printf("Client disconnected!\n");
		
	}

	//Close Socket
	closesocket(listenSock);

	//Terminate WinSock
	WSACleanup();

	_getch();

}

void prc_mess(state_client st_cl,account *acc,sockaddr_in claddr) {
	while (1) {
		int ret;
		char buff[2048];
		message mes;
		ret = recv(st_cl.connSock, buff, 2048, 0);
		if (ret == SOCKET_ERROR) {
			if (ret == SOCKET_ERROR) {
				printf("Client disconnected!\n");
			}
			break;
		}
		else {
			buff[ret] = '\0';
			memcpy(&mes, buff, sizeof(message));
			switch (mes.TYPE)
			{
			case 1: { //Specify user
				printf("Receive from client [%s:%d]: USER %s\n",
					inet_ntoa(claddr.sin_addr),
					ntohs(claddr.sin_port), mes.mes);
				if (strcmp(st_cl.state, "Un-authenticated") == 0) {
					int check = check_acc(acc, mes.mes, 10);
					if (check == 0) {//not found id
						ret = send(st_cl.connSock, "11", 2, 0);
						if (ret == SOCKET_ERROR) {
							printf("ERROR: %", WSAGetLastError());
							continue;
						}
					}
					else {//found id
						ret = send(st_cl.connSock, "01", 2, 0);
						if (ret == SOCKET_ERROR) {
							printf("ERROR: %", WSAGetLastError());
							continue;
						}
						st_cl.state = "Specified-user";
						memcpy(&st_cl.acc, &acc[check - 1], sizeof(account));
					}
				}
				else {
					ret = send(st_cl.connSock, "21", 2, 0);
					if (ret == SOCKET_ERROR) {
						printf("ERROR: %", WSAGetLastError());
						continue;
					}
				}
				break;
			}
			case 2: {	//Mathed password
				printf("Receive from client [%s:%d]: PASS: %s\n",
					inet_ntoa(claddr.sin_addr),
					ntohs(claddr.sin_port), mes.mes);
				if (strcmp(st_cl.state, "Specified-user") == 0) {
					if (check_pass(mes.mes, st_cl.acc)) {
						ret = send(st_cl.connSock, "02", 2, 0);
						if (ret == SOCKET_ERROR) {
							printf("ERROR: %\n", WSAGetLastError());
							continue;
						}
						st_cl.state = "Authenticated";
					}
					else {
						ret = send(st_cl.connSock, "12", 2, 0);
						if (ret == SOCKET_ERROR) {
							printf("ERROR: %\n", WSAGetLastError());
							continue;
						}
					}
				}
				else {
					ret = send(st_cl.connSock, "22", 2, 0);
					if (ret == SOCKET_ERROR) {
						printf("ERROR: %\n", WSAGetLastError());
						continue;
					}
				}
				break;
			}
			case 3: {	//LOGOUT
				printf("Receive from client [%s:%d]: LOGOUT\n",
					inet_ntoa(claddr.sin_addr),
					ntohs(claddr.sin_port));
				if (strcmp(st_cl.state, "Authenticated") == 0) {
					ret = send(st_cl.connSock, "03", 2, 0);
					if (ret == SOCKET_ERROR) {
						printf("ERROR: %\n", WSAGetLastError());
						continue;
					}
					st_cl.state = "Un-authenticated";
				}
				else {
					ret = send(st_cl.connSock, "13", 2, 0);
					if (ret == SOCKET_ERROR) {
						printf("ERROR: %\n", WSAGetLastError());
						continue;
					}
				}
				break;
			}
			default:
				break;
			}
		}
		//free(&mes);
	}
	shutdown(st_cl.connSock, SD_SEND);
	closesocket(st_cl.connSock);
}
