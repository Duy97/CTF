#pragma once
#include<stdio.h>
#include<conio.h>
#include<tchar.h>
#include<ctype.h>

struct message {
	char Opcode[1];
	u_short length;
	char Payload[1024];
};

struct data_thread
{
	SOCKET connSock;
	sockaddr_in clientAddr;
};

bool check_number(char *buff) {
	int check = 0;
	for (int i = 0; i < strlen(buff); i++) {
		if (!isdigit(buff[i])) {
			check = 1;
			break;
		}
	}
	if (check == 0) return true;
	else return false;
}

//function encode
byte *encode(byte *a, byte k, int n) {
	byte *b = new byte[n];
	for (int i = 0; i < n; i++) {
		b[i] = a[i] + k;
	}
	return b;
}



bool remove_file(char *url) {
	int ret = remove(url);
	if (ret == 0) return true;
	else return false;
}

int getfilesize(char *url) {
	FILE *f = fopen(url, "r");
	if (f == NULL) return 0;
	else {
		//get file size
		fseek(f, 0, SEEK_END);
		int Size = ftell(f);
		fseek(f, 0, SEEK_SET);
		fclose(f);
		return Size;
	}
}

bool check_file(char *url) {
	FILE *f = fopen(url, "r");
	if (f == NULL)
	{
		return false;
	}
	else {
		fclose(f);
		return true;
	}
}

bool encode_file(char *url_dest, char *url_source, byte key) {
	if (check_file(url_source)) {
		int size = getfilesize(url_source);
		printf("Size of source file: %d\n", size);
		FILE *f = fopen(url_source, "rb");
		if (size < 1000) {
			FILE *f1 = fopen(url_dest, "wb");
			byte *buffer = new byte[size];
			byte *dest = new byte[size];
			int read = fread_s(buffer, size, sizeof(char), size, f);
			buffer[read] = '\0';
			memcpy(dest, encode(buffer, key, size), size);
			dest[size] = '\0';
			fwrite(dest, 1, size, f1);
			fflush(f1);
			fclose(f1);
		}
		else {
			int count_byte = 0;
			FILE *f1 = fopen(url_dest, "wb");
			while (count_byte < size) {
				byte *buffer = new byte[1001];
				byte *dest = new byte[1000];
				int read = fread_s(buffer, 1000, sizeof(char), 1000, f);
				count_byte += read;
				buffer[read] = '\0';
				memcpy(dest, encode(buffer, key, read), read);
				dest[read] = '\0';
				fwrite(dest, 1, read, f1);
				fflush(f1);
			}
			fclose(f1);
		}
		fclose(f);
		int size1 = getfilesize(url_dest);
		printf("Size of dest file: %d\n", size1);
		if (size == size1) return true;
		else return false;
	}
	else return false;
}

bool create_file(char *url) {
	int check = 0;
	FILE *f = fopen(url, "w");
	if (f == NULL) check = 1;
	fclose(f);
	if (check == 1) return false;
	else return true;
}