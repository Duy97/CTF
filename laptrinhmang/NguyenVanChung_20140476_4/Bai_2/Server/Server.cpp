#include <stdio.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include "mylibrary.h"
#include <process.h>
#define SERVER_ADDR "127.0.0.1"
#define BUFF_SIZE 2048

#pragma comment (lib,"Ws2_32")

void process_file(SOCKET connSock, char *tem_file, char *result_file, char *id, int port, int key);
void receive_file(SOCKET client, FILE *fp);
void communicate(SOCKET connSock, sockaddr_in clientAddr);
void sendER(SOCKET connsock);
void sendfl(SOCKET connsock);
int send_file(SOCKET connSock, char *url_file);

unsigned __stdcall f_thread(void *param) {
	data_thread dt_th;
	memcpy(&dt_th, param, sizeof(data_thread));
	communicate(dt_th.connSock, dt_th.clientAddr);

	//shutdown socket
	shutdown(dt_th.connSock, SD_SEND);
	//close socket
	closesocket(dt_th.connSock);
	return 0;
}


int _tmain(int argc, char *argv[]) {

	//Step 1: Initiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData)) {
		printf("Version is not support!\n");
		return 0;
	}
	//Step 2: Contruct socket
	SOCKET listenSock;
	listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	//Step 3: Bind address to socket
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

	//Process command line parameters
	if (argc == 3) {
		//check command line parameters
		if (strcmp(argv[1], "-p") == 0) {
			//check port
			if (check_number(argv[2])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[2]));
				//Bind
				if (bind(listenSock, (sockaddr*)&serverAddr, sizeof(sockaddr))) {
					printf("ERROR: Can not bind this address!");
					return 0;
				}
				//Bind success
				printf("Server started in [%s:%s]\n", SERVER_ADDR, argv[2]);
			}
			else {
				printf("ERROR : Command line parameter is wrong!");
				return 0;
			}
		}
		else {
			printf("ERROR : Command line parameter is wrong!");
			return 0;
		}
	}
	else {
		printf("ERROR : Command line parameter is wrong!");
		return 0;
	}

	//Step 4: Listen request from client
	if (listen(listenSock, 10)) {
		printf("Error! Cannot listen.");
		_getch();
		return 0;
	}

	//Step 5: Communicate with client

	sockaddr_in clientAddr;
	int ret, clientAddrLen = sizeof(clientAddr);
	while (1) {
		//specified data for thread function
		data_thread dt_th;
		SOCKET connSock;
		connSock = accept(listenSock, (sockaddr*)&clientAddr, &clientAddrLen);
		dt_th.connSock = connSock;
		dt_th.clientAddr = clientAddr;

		//begin thread function
		_beginthreadex(0, 0, f_thread, (void*)&dt_th, 0, 0);

	}


	//close listenSock
	closesocket(listenSock);

	//Terminate WinSock
	WSACleanup();

	//end Server
	_getch();

}


void communicate(SOCKET connSock, sockaddr_in clientAddr) {
	//Get id and port of client
	char *id = inet_ntoa(clientAddr.sin_addr);
	int port = (int)ntohs(clientAddr.sin_port);

	//Create url of itemporary file and url of result file
	char tem_file[200];
	sprintf(tem_file, "../File_Of_Server/itemporary%i.txt", port);
	char result_file[200];
	sprintf(result_file, "../File_Of_Server/result%i.txt", port);

	//communicate
	int ret;
	char *buff = new char[1028];
	//receive message of request from client
	ret = recv(connSock, buff, 1028, 0);
	if (ret == SOCKET_ERROR) {
		printf("Client [%s:%d] disconnected!\n", id, port);
		return;
	}
	else {
		buff[ret] = '\0';
		message mes;
		memcpy(&mes, buff, sizeof(message));
		int key = atoi(mes.Payload);
		//dissect Opcode
		switch (atoi(mes.Opcode))
		{
		//Encode
		case 0: {
			printf("Request from [%s:%d]: ENCODE\n", id, port);
			printf("Key: %d\n", key);
			process_file(connSock, tem_file, result_file, id, port, key);
			break;
		}
		//Decode
		case 1: {
			printf("Request from [%s:%d]: DECODE\n", id, port);
			printf("Key: %d\n", key);
			process_file(connSock, tem_file, result_file, id, port, -key);
			break;
		}
		default:
			printf("Client [%s:%d] disconnected!\n", id, port);
			return;
		}
	}
}

//Send message notify Error
void sendER(SOCKET connsock) {
	message meserr;
	int ret;
	memcpy(meserr.Opcode, "3", 1);
	ret = send(connsock, (char *)&meserr, sizeof(message), 0);
	if (ret == SOCKET_ERROR) {
		printf("ERROR: %", WSAGetLastError());
	}
	return;
}

//Send message notify finish
void sendfl(SOCKET connsock) {
	message mes;
	int ret;
	mes.length = 0;
	memcpy(mes.Opcode, "2", 1);
	ret = send(connsock, (char*)&mes, sizeof(message), 0);
	if (ret == SOCKET_ERROR) {
		printf("ERROR: %", WSAGetLastError());
	}
	return;
}

//send file
int send_file(SOCKET client, char *url_file) {
	int ret;
	int size = getfilesize(url_file);
	//open file
	FILE *fr = fopen(url_file, "rb");
	//send file
	if (size < 1024) { //small file
		message mesf;
		memcpy(mesf.Opcode, "2", 1);
		memcpy(&mesf.length, &size, 2);
		byte *buffer = new byte[size];
		fread_s(buffer, size, sizeof(char), size, fr);
		memcpy(mesf.Payload, buffer, size);
		ret = send(client, (char *)&mesf, sizeof(message), 0);
		if (ret == SOCKET_ERROR) {
			printf("ERROR: %", WSAGetLastError());
			sendER(client);
			fclose(fr);
			return 0;
		}
		sendfl(client);
		fclose(fr);
		return size;
	}
	else {//big file
		int count_byte = 0;
		while (count_byte < size) {
			message mesf;
			memcpy(mesf.Opcode, "2", 1);
			byte *buffer = new byte[1024];
			int read = fread_s(buffer, 1024, sizeof(char), 1024, fr);
			mesf.length = (u_short)read;
			memcpy(mesf.Payload, buffer, read);
			ret = send(client, (char *)&mesf, sizeof(message), 0);
			if (ret == SOCKET_ERROR) {
				printf("ERROR: %", WSAGetLastError());
				sendER(client);
				fclose(fr);
				return 0;
			}
			count_byte += read;
			free(buffer);
		}
		//end send file
		sendfl(client); //send message notify finish
		fclose(fr);
		return count_byte;
	}
}

void receive_file(SOCKET client, FILE *fp) {
	int check1 = 0, ret;
	while (check1 == 0) {
		char *buffer = new char[1028];
		ret = recv(client, buffer, 1028, 0);
		if (ret == SOCKET_ERROR) {
			printf("Can not receive!\n");
			check1 = 1;
		}
		message *mes1 = new message[1];
		memcpy(mes1, buffer, sizeof(message));
		switch (atoi(mes1[0].Opcode))
		{
		case 2: {
			switch (mes1[0].length)
			{
			case 0: {
				//end receive file
				printf("Receive file from Server: Complete!\n");
				check1 = 1;
				break;
			}
			default: {
				//save_file
				fwrite((byte*)mes1[0].Payload, 1, (int)mes1[0].length, fp);
				fflush(fp);
				break;
			}
			}
			break;
		}
		case 3: {
			//receive Error from server
			printf("Receive From Server: ERROR.\n");
			check1 = 1;
			break;

		}
		default:
			break;
		}
		free(mes1);
		free(buffer);
	}//end while
}

void process_file(SOCKET connSock,char *tem_file,char *result_file,char *id,int port,int key) {
	int ret;
	//create itemporary file and result file
	create_file(tem_file);
	create_file(result_file);

	//receive file from client
	FILE *fp = fopen(tem_file, "wb");
	receive_file(connSock, fp);
	fclose(fp);//close file
	printf("Size of file receive from [%s:%d]  %d\n", id, port, getfilesize(tem_file));
	//process file
	if (encode_file(result_file, tem_file, key)) {
		printf("Process file Complete!\n");
		//send encode file
		int sent_size = send_file(connSock, result_file);
		printf("Sent: %d byte to client [%s:%d]\n", sent_size, id, port);
	}
	else {
		//send error
		printf("Can not encode!\n");
		sendER(connSock);
	}
	//remove file
	remove_file(tem_file);
	remove_file(result_file);
}