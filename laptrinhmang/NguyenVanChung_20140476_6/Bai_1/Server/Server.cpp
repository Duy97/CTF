//#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "stdafx.h"
#include "Server.h"
#include <WinSock2.h>
#include <winsock.h>
#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include "mylibrary.h"

#define WM_SOCKET WM_USER + 1
#define SERVER_PORT 5500
#define SERVER_ADDR "127.0.0.1"
#define MAX_CLIENT 1024
#define BUFF_SIZE 2048

#pragma comment(lib, "Ws2_32.lib")
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
HWND                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);

//function of processing

account *create_acc(int n) {
	account *acc = new account[n];
	for (int i = 0; i < n; i++) {
		account new_acc;
		strcpy(new_acc.id, "chungnv");
		strcpy(new_acc.psw, "password");
		char c[100];
		sprintf(c, "%i", i);
		strcat(new_acc.id, c);
		strcat(new_acc.psw, c);
		new_acc.failed_login = 0;
		acc[i] = new_acc;
	}
	return acc;
}

int receiveData(SOCKET, char *, int, int);
int sendData(SOCKET, char *, int, int);
void prc_mess(state_client *, char *, account *);
state_client st_client[MAX_CLIENT];
SOCKET	listenSock;
HWND	textEdit, button;
account *acc = create_acc(10);


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	//Get argv,argc
	int argc;
	char **argv = cut_string(lpCmdLine, &argc, " ");

	MSG msg;
	HWND serverWindow;

	MyRegisterClass(hInstance);

	serverWindow = InitInstance(hInstance, nShowCmd);
	if (serverWindow == NULL)
	{
		return 0;
	}

	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData))
	{
		MessageBox(serverWindow, L"Error WSAStartUp", L"Error", MB_OK | MB_ICONERROR);
		return FALSE;
	}

	listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	WSAAsyncSelect(listenSock, serverWindow, WM_SOCKET, FD_ACCEPT | FD_CLOSE | FD_READ);

	//Step 3: Bind address to socket
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	//serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

	if (argc == 2) {
		//check command line parameters
		if (strcmp(argv[0], "-p") == 0) {
			//check port
			if (check_number(argv[1])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[1]));
				//Bind
				if (bind(listenSock, (sockaddr*)&serverAddr, sizeof(sockaddr))) {
					MessageBox(serverWindow, L"Can not bind serverAddr!", L"ERROR!", MB_OK);
					return 0;
				}
				//Bind success
			}
			else {
				MessageBox(serverWindow, L"Command line parameter is wrong!", L"ERROR!", MB_OK);
				return 0;
			}
		}
		else {
			MessageBox(serverWindow, L"Command line parameter is wrong!", L"ERROR!", MB_OK);
			return 0;
		}
	}
	else {
		MessageBox(serverWindow, L"Command line parameter is wrong!", L"ERROR!", MB_OK);
		return 0;
	}

	//Step 4: Listen request from client
	if (listen(listenSock, 10)) {
		//printf("Error! Cannot listen.");
		_getch();
		return 0;
	}

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 1;
}
//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"WindowClass";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
HWND InitInstance(HINSTANCE hInstance, int nCmdShow)
{

	int i;
	for (i = 0; i < MAX_CLIENT; i++)
	{
		//Initialize value for st_client
		st_client[i].connSock = 0;
		st_client[i].state = "Un-authenticated";
	}
	HWND hWnd = CreateWindow(L"WindowClass", L"WSAAsyncselect", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		int error = GetLastError();
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return hWnd;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) //wParam phat sinh thong diep
{
	SOCKET connSock;
	sockaddr_in clientAddr;
	int ret, clientAddrLen = sizeof(clientAddr), i;
	char recvBuff[BUFF_SIZE], sendBuff[BUFF_SIZE];
	switch (message)
	{
	case  WM_SOCKET:
	{
		if (WSAGETASYNCERROR(lParam)) //get asyn error
		{
			for (i = 0; i < MAX_CLIENT; i++)
			{
				if (st_client[i].connSock == (SOCKET)wParam)
				{
					//Close socket and reset value of st_client
					closesocket(st_client[i].connSock);
					st_client[i].connSock = 0;
					st_client[i].state = "Un-authenticated";
					continue;
				}
			}
		}
		switch (WSAGETSELECTEVENT(lParam))
		{
		case FD_ACCEPT:
		{
			connSock = accept((SOCKET)wParam, (sockaddr*)&clientAddr, &clientAddrLen);
			if (connSock == INVALID_SOCKET)
			{
				break;
			}
			for (i = 0; i < MAX_CLIENT; i++)
			{
				if (st_client[i].connSock == 0)
				{
					st_client[i].connSock = connSock;
					WSAAsyncSelect(st_client[i].connSock, hWnd, WM_SOCKET, FD_READ | FD_CLOSE);
					break;
				}
			}
			if (i == MAX_CLIENT)
			{
				MessageBox(hWnd, L"Too many client", L"Notice", MB_OK);
			}
			break;
		}
		case  FD_READ:
		{
			for (i = 0; i < MAX_CLIENT; i++)
			{
				if (st_client[i].connSock != (SOCKET)wParam)
				{
					continue;
				}
				//receive data from client
				ret = recv(st_client[i].connSock, recvBuff, BUFF_SIZE, 0);
				if (ret > 0)
				{
					recvBuff[ret] = 0;
					//process message
					prc_mess(&st_client[i], recvBuff, acc);
					break;
				}
			}
			break;
		}
		case FD_CLOSE: //Client disconnected
		{
			for (int i = 0; i < MAX_CLIENT; i++)
			{
				if (st_client[i].connSock == (SOCKET)wParam)
				{
					//Close socket and reset value of st_client
					closesocket(st_client[i].connSock);
					st_client[i].connSock = 0;
					st_client[i].state = "Un-authenticated";
					break;
				}
			}
			break;
		}
		}
		break;
	}
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		shutdown(listenSock, SD_BOTH);
		closesocket(listenSock);
		WSACleanup();
		return 0;
	}
	case WM_CLOSE:
	{
		DestroyWindow(hWnd);
		shutdown(listenSock, SD_BOTH);
		closesocket(listenSock);
		WSACleanup();
		return 0;
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void processData(char *in, char * out)
{
	strcpy_s(out, BUFF_SIZE, in);
}

/*The process message function: process message and send answer to client*/
void prc_mess(state_client *state_cl, char *buff, account *acc) {
	state_client st_cl;
	memcpy(&st_cl, state_cl, sizeof(state_client));
	message mes;
	memcpy(&mes, buff, sizeof(message));
	switch (mes.TYPE)
	{
	case 1: { //Specify user
		if (strcmp(st_cl.state, "Un-authenticated") == 0) {
			int check = check_acc(acc, mes.mes, 10);
			if (check == 0) {//not found id
				sendData(st_cl.connSock, "11", 2, 0);
			}
			else {//found id
				sendData(st_cl.connSock, "01", 2, 0);
				st_cl.state = "Specified-user";
				memcpy(&st_cl.acc, &acc[check - 1], sizeof(account));
			}
		}
		else {
			sendData(st_cl.connSock, "21", 2, 0);
		}
		break;
	}
	case 2: {	//Mathed password
		if (strcmp(st_cl.state, "Specified-user") == 0) {
			if (check_pass(mes.mes, st_cl.acc)) {
				sendData(st_cl.connSock, "02", 2, 0);
				st_cl.state = "Authenticated";
			}
			else {
				sendData(st_cl.connSock, "12", 2, 0);
			}
		}
		else {
			sendData(st_cl.connSock, "22", 2, 0);

		}
		break;
	}
	case 3: {	//LOGOUT
		if (strcmp(st_cl.state, "Authenticated") == 0) {
			sendData(st_cl.connSock, "03", 2, 0);
			st_cl.state = "Un-authenticated";
		}
		else {
			sendData(st_cl.connSock, "13", 2, 0);
		}
		break;
	}
	default:
		break;
	}
	memcpy(state_cl, &st_cl, sizeof(state_client));
}

/*The recv() wrapper function*/
int receiveData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = recv(s, buff, size, flags);
	if (n <= 0) {
		//printf("\nOne client disconnected.");
	}
	else {
		buff[n] = '\0';
		//printf("\nreceive one message!");
	}
	return n;
}


/*The send() wrapper function*/
int sendData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = send(s, buff, size, flags);
	//if (n <= 0) printf("\nsend error.");
	return n;
}