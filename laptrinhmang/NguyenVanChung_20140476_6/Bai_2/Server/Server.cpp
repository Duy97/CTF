//#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "stdafx.h"
#include "Server.h"
#include <WinSock2.h>
#include <winsock.h>
#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include "mylibrary.h"

#define WM_SOCKET WM_USER + 1
#define SERVER_PORT 5500
#define SERVER_ADDR "127.0.0.1"
#define MAX_CLIENT 1024
#define BUFF_SIZE 1028

#pragma comment(lib, "Ws2_32.lib")
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
HWND                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);

//function of processing
void sendER(SOCKET);
int receiveData(SOCKET, char *, int, int);
int sendData(SOCKET, char *, int, int);
void prc_mess(state_client *, char *, HWND);
int send_to_client(state_client *, HWND);
state_client st_client[MAX_CLIENT];
SOCKET	listenSock;
HWND	textEdit, button;



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	int argc;
	char **argv = cut_string(lpCmdLine, &argc, " ");

	MSG msg;
	HWND serverWindow;

	MyRegisterClass(hInstance);

	serverWindow = InitInstance(hInstance, nShowCmd);
	if (serverWindow == NULL)
	{
		return 0;
	}

	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData))
	{
		MessageBox(serverWindow, L"Error WSAStartUp", L"Error", MB_OK | MB_ICONERROR);
		return FALSE;
	}

	listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	WSAAsyncSelect(listenSock, serverWindow, WM_SOCKET, FD_ACCEPT | FD_CLOSE | FD_READ);

	//Step 3: Bind address to socket
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	//serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

	if (argc == 2) {
		//check command line parameters
		if (strcmp(argv[0], "-p") == 0) {
			//check port
			if (check_number(argv[1])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[1]));
				//Bind
				if (bind(listenSock, (sockaddr*)&serverAddr, sizeof(sockaddr))) {
					MessageBox(serverWindow, L"Can not bind serverAddr!", L"ERROR!", MB_OK);
					return 0;
				}
				//Bind success
			}
			else {
				MessageBox(serverWindow, L"Command line parameter is wrong!", L"ERROR!", MB_OK);
				return 0;
			}
		}
		else {
			MessageBox(serverWindow, L"Command line parameter is wrong!", L"ERROR!", MB_OK);
			return 0;
		}
	}
	else {
		MessageBox(serverWindow, L"Command line parameter is wrong!", L"ERROR!", MB_OK);
		return 0;
	}

	//Step 4: Listen request from client
	if (listen(listenSock, 10)) {
		//printf("Error! Cannot listen.");
		_getch();
		return 0;
	}

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 1;
}
//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"WindowClass";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
HWND InitInstance(HINSTANCE hInstance, int nCmdShow)
{

	int i;
	for (i = 0; i < MAX_CLIENT; i++)
	{
		//Initialize value for st_client
		st_client[i].connSock = 0;
		st_client[i].state = "receive";
		st_client[i].key = 0;
		st_client[i].fp = NULL;
		st_client[i].tem_file = "";
		st_client[i].result_file = "";
	}
	HWND hWnd = CreateWindow(L"WindowClass", L"WSAAsyncselect", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		int error = GetLastError();
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return hWnd;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) //wParam
{
	SOCKET connSock;
	sockaddr_in clientAddr;
	int ret, clientAddrLen = sizeof(clientAddr), i;
	char *recvBuff;
	switch (message)
	{
	case  WM_SOCKET:
	{
		if (WSAGETASYNCERROR(lParam))
		{
			for (i = 0; i < MAX_CLIENT; i++)
			{
				if (st_client[i].connSock == (SOCKET)wParam)
				{
					//Close socket and reset value of st_client
					close_file(st_client[i].fp); //close file point
					closesocket(st_client[i].connSock); //close socket
					remove_file(st_client[i].tem_file); //Remove item file
					remove_file(st_client[i].result_file); //Remove result file
					//Reset value of st_client
					st_client[i].connSock = 0;
					st_client[i].key = 0;
					st_client[i].state = "receive";
					close_file(st_client[i].fp);
					st_client[i].tem_file = "";
					st_client[i].result_file = "";
					break;
				}
			}
		}
		switch (WSAGETSELECTEVENT(lParam))
		{
		case FD_ACCEPT:
		{
			connSock = accept((SOCKET)wParam, (sockaddr*)&clientAddr, &clientAddrLen);
			char *id = inet_ntoa(clientAddr.sin_addr);
			int port = (int)ntohs(clientAddr.sin_port);
			char *tem_file = new char[200];
			sprintf(tem_file, "../File_Of_Server/itemporary%i.txt", port);
			char *result_file = new char[200];
			sprintf(result_file, "../File_Of_Server/result%i.txt", port);
			if (connSock == INVALID_SOCKET)
			{
				break;
			}
			for (i = 0; i < MAX_CLIENT; i++)
			{
				if (st_client[i].connSock == 0)
				{
					st_client[i].connSock = connSock;
					st_client[i].tem_file = tem_file; //add url item file for client
					st_client[i].result_file = result_file; // add url result file for client
					WSAAsyncSelect(st_client[i].connSock, hWnd, WM_SOCKET, FD_READ | FD_CLOSE);
					break;
				}
			}
			if (i == MAX_CLIENT)
			{
				MessageBox(hWnd, L"Too many client", L"Notice", MB_OK);
			}
			break;
		}
		case  FD_READ:
		{
			for (i = 0; i < MAX_CLIENT; i++)
			{
				if (st_client[i].connSock != (SOCKET)wParam)
				{
					continue;
				}
				recvBuff = new char[BUFF_SIZE];
				//recieve data from client
				ret = recv(st_client[i].connSock, recvBuff, BUFF_SIZE, 0);
				if (ret > 0)
				{
					//process message
					prc_mess(&st_client[i], recvBuff, hWnd);
					break;
				}
			}
			break;
		}
		case FD_WRITE:
		{
			for (i = 0; i < MAX_CLIENT; i++)
			{
				if (st_client[i].connSock != (SOCKET)wParam)
				{
					continue;
				}
				if (strcmp(st_client[i].state, "sendfile") == 0) {
					ret = send_to_client(&st_client[i], hWnd);
					if (ret == 0) {
						st_client[i].state = "receive"; // change state to receive file
						close_file(st_client[i].fp); // close file point
						remove_file(st_client[i].tem_file);//Remove item file
						remove_file(st_client[i].result_file);//Remove result file
					}
					break;
				}
			}
			break;
		}
		case FD_CLOSE: // Client closed
		{
			for (int i = 0; i < MAX_CLIENT; i++)
			{
				if (st_client[i].connSock == (SOCKET)wParam)
				{
					//Close socket and reset value of st_client
					close_file(st_client[i].fp); // close file point
					closesocket(st_client[i].connSock);  //close socket
					remove_file(st_client[i].tem_file); //Remove item file
					remove_file(st_client[i].result_file); //Remove result file
					//Reset value of st_client
					st_client[i].connSock = 0;
					st_client[i].key = 0;
					st_client[i].state = "receive";
					close_file(st_client[i].fp);
					st_client[i].tem_file = "";
					st_client[i].result_file = "";
					break;
				}
			}
			break;
		}
		}
		break;
	}
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		shutdown(listenSock, SD_BOTH);
		closesocket(listenSock);
		WSACleanup();
		return 0;
	}
	case WM_CLOSE:
	{
		DestroyWindow(hWnd);
		shutdown(listenSock, SD_BOTH);
		closesocket(listenSock);
		WSACleanup();
		return 0;
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

/*The process message function: process message and send answer to client*/
void prc_mess(state_client *st_client, char *buff, HWND hWnd) {
	state_client st_cl;
	memcpy(&st_cl, st_client, sizeof(state_client));
	message mes;
	memcpy(&mes, buff, sizeof(message));
	switch (atoi(mes.Opcode)) {
	case 0: {
		//create itemporary file and result file
		create_file(st_cl.tem_file);
		create_file(st_cl.result_file);
		st_cl.fp = fopen(st_cl.tem_file, "wb");
		//save key
		st_cl.key = atoi(mes.Payload);
		//change state
		st_cl.state = "ENCODE";
		break;
	}
	case 1: {
		//create itemporary file and result file
		create_file(st_cl.tem_file);
		create_file(st_cl.result_file);
		st_cl.fp = fopen(st_cl.tem_file, "wb");
		//save key
		st_cl.key = atoi(mes.Payload);
		//change state
		st_cl.state = "DECODE";
		break;
	}
	case 2: {
		switch (mes.length)
		{
		case 0: {
			//end receive file
			close_file(st_cl.fp);
			close_file(st_client[0].fp);
			if (strcmp(st_cl.state, "ENCODE") == 0) {//ENCODE file
				if (encode_file(st_cl.result_file, st_cl.tem_file, st_cl.key)) {
					st_cl.state = "sendfile";
					WSAAsyncSelect(st_client[0].connSock, hWnd, WM_SOCKET, FD_WRITE | FD_CLOSE);
				}
				else sendER(st_cl.connSock);
			}
			else {//DECODE file
				if (encode_file(st_cl.result_file, st_cl.tem_file, (-st_cl.key))) {
					st_cl.state = "sendfile";	
					WSAAsyncSelect(st_client[0].connSock, hWnd, WM_SOCKET, FD_WRITE | FD_CLOSE);
				}
				else sendER(st_cl.connSock);
			}
			st_cl.fp = fopen(st_cl.result_file, "rb");//open result file
			break;
		}
		default: {
			//save_file
			fwrite(mes.Payload, 1, (int)mes.length, st_cl.fp);
			fflush(st_cl.fp);
			break;
		}
		}
	}
	case 3: {
		break;
	}
	default:
		break;
	}
	memcpy(st_client, &st_cl, sizeof(state_client));
}

/*send file to server*/
int send_to_client(state_client *st_client, HWND hWnd) {
	state_client st_cl;
	memcpy(&st_cl, st_client, sizeof(state_client));
	message mes;
	memcpy(mes.Opcode, "2", 1);
	int read;
	read = fread_s(mes.Payload, 1024, sizeof(char), 1024, st_cl.fp);
	mes.length = read;
	sendData(st_cl.connSock, (char *)&mes, 1028, 0);
	WSAAsyncSelect(st_client[0].connSock, hWnd, WM_SOCKET, FD_WRITE | FD_CLOSE);
	memcpy(st_client, &st_cl, sizeof(state_client));
	return read;
}

//Send message notify Error
void sendER(SOCKET connsock) {
	message meserr;
	int ret;
	memcpy(meserr.Opcode, "3", 1);
	ret = send(connsock, (char *)&meserr, sizeof(message), 0);
	if (ret == SOCKET_ERROR) {
		//printf("ERROR: %", WSAGetLastError());
	}
	return;
}


/*The recv() wrapper function*/
int receiveData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = recv(s, buff, size, flags);
	if (n <= 0) {
		//printf("\nOne client disconnected.");
	}
	else {
		buff[n] = '\0';
	}
	return n;
}

/*The send() wrapper function*/
int sendData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = send(s, buff, size, flags);
	//if (n <= 0) printf("\nsend error.");
	return n;
}