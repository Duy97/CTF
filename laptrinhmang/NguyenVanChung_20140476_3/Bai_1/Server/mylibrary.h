#pragma once
#include<stdio.h>
#include<conio.h>
#include<tchar.h>
#include<ctype.h>
#include<WinSock2.h>

struct account {
	char id[100]; //id
	char psw[100]; //password
	int failed_login; // count the failed logins of id
};

struct state_client {
	SOCKET connSock; // socket connect from client to server
	char* state; //state of client
	account acc; //account of client use to login
	int failed_login; // count the gailed logins of client
};


struct message {
	int TYPE; // message type
	char mes[100]; // message
};

bool check_number(char *buff) {
	int check = 0;
	for (int i = 0; i < strlen(buff); i++) {
		if (!isdigit(buff[i])) {
			check = 1;
			break;
		}
	}
	if (check == 0) return true;
	else return false;
}

int check_acc(account *acc, char *id, int n) {
	int check = 0;
	for (int i = 0; i < n; i++) {
		if (strcmp(acc[i].id, id) == 0) {
			check = i + 1;
			break;
		}
	}
	return check;
}

bool check_pass(char *pas, account acc) {
	if (strcmp(pas, acc.psw) == 0) return true;
	else return false;
}

