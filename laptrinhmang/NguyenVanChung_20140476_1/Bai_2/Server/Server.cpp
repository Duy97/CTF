#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
const unsigned BUFF_SIZE = 2048;
#pragma comment (lib,"Ws2_32")
int main() {
	//Step1: Inittiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData))
		printf("Version can not support!");
	//Step2: Construct socket
	SOCKET server;
	server = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	//Step3: Bind address to socket
	//Input IP 7 PORT and Bind socket
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	char ipv4[30];
	int port;
	char s;
	int check = 0;
	//check input
	do {
		printf("IP Server: ");
		gets_s(ipv4,30);
		printf("Port: ");
		scanf("%d", &port);
		gets_s(&s, 1);
		serverAddr.sin_port = htons((short)port);
		serverAddr.sin_addr.s_addr = inet_addr(ipv4);
		if (bind(server, (sockaddr *)&serverAddr, sizeof(serverAddr))) {
			printf("Error! Can not bind this address.\n");
			printf("Yeu cau nhap lai!\n");
			check = 1;
		}
		else {
			printf("Server started!\n");
		}
	} while (check == 1);//check complete
	//Step 4: Communication with client
	sockaddr_in clientAddr, address;
	char buff[BUFF_SIZE];
	char error[BUFF_SIZE] = "Can not get address info!";
	int ret,rc, clientAddrLen = sizeof(clientAddr);
	while (1) {
		ret = recvfrom(server, buff, BUFF_SIZE, 0, (sockaddr *)&clientAddr, &clientAddrLen);
		if (ret == SOCKET_ERROR) {
			printf("Error : %", WSAGetLastError());
		}else {
			buff[ret] = '\0';
			printf("Receiver from client[%s:%d] %s\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port), buff);
		}
		//Get address info
		addrinfo *result;
		rc = getaddrinfo(buff, "http", NULL, &result);
		if (rc == 0) {
			//get the first address
			memcpy(&address, result->ai_addr, result->ai_addrlen);
			//send address to server
			ret = sendto(server, (char *)inet_ntoa(address.sin_addr), strlen((char *)inet_ntoa(address.sin_addr)), 0, (sockaddr *)&clientAddr, clientAddrLen);
			if (ret == SOCKET_ERROR) {
				printf("Error: %", WSAGetLastError());
			}
		}else {
			//send erorr to server
			ret = sendto(server, error, strlen(error), 0, (sockaddr *)&clientAddr, clientAddrLen);
			if (ret == SOCKET_ERROR) {
				printf("Error: %", WSAGetLastError());
			}
		}
		freeaddrinfo(result);
	}//end while
	
	//Close Socket
	closesocket(server);

	//Terminate WinSock
	WSACleanup();
}