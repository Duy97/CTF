#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
const unsigned BUFF_SIZE = 2048;
#pragma comment (lib,"Ws2_32")
int main() {
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData))
		printf("Version is not supported.\n");
	printf("Client started!\n");
	printf("Nhap Thong Tin Server!\n");
	//Step 2: Construct socket
	SOCKET client;
	client = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	//(optional) Set time-out for receiving
	int tv = 10000; //Time-out interval: 10000ms
	setsockopt(client, SOL_SOCKET, SO_RCVTIMEO,
		(const char*)(&tv), sizeof(int));
	//Step 3: Specify server address
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	//Input IP and PORT for connect to server
	char ipv4[30];
	int port;
	char s;
	printf("IP Server: ");
	gets_s(ipv4,30);
	printf("Port: ");
	scanf("%d", &port);
	gets_s(&s, 1);
	serverAddr.sin_port = htons((short)port);
	serverAddr.sin_addr.s_addr = inet_addr(ipv4);
	//Step 4: Communicate with server
	char buff[BUFF_SIZE];
	int ret, serverAddrLen = sizeof(serverAddr);
	while (1) {
		//Send message
		printf("Send Domain or IP to server: ");
		gets_s(buff,BUFF_SIZE);
		ret = sendto(client, buff, strlen(buff), 0,
			(sockaddr *)&serverAddr, serverAddrLen);
		if (ret == SOCKET_ERROR)
			printf("Error! Cannot send mesage.");
		//Receive echo message
		ret = recvfrom(client, buff, BUFF_SIZE, 0,
			(sockaddr *)&serverAddr, &serverAddrLen);
		if (ret == SOCKET_ERROR) {
			if (WSAGetLastError() == WSAETIMEDOUT)
				printf("Time-out!");
			else printf("Error! Cannot receive message.");
			_getch();
		}
		else {
			buff[ret] = '\0';
			printf("Receive from server[%s:%d] % s\n",
				inet_ntoa(serverAddr.sin_addr),
				ntohs(serverAddr.sin_port), buff);
		}
	} //end while
	  //Step 5: Close socket
	closesocket(client);
	//Step 6: Terminate Winsock
	WSACleanup();
}