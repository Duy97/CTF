#include <stdio.h>
#include <conio.h>
#include <iostream>
#include <tchar.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#define BUFF_SIZE 2048
#pragma comment (lib,"Ws2_32")

struct message {
	int TYPE; // message type
	char mes[1024]; // message
};

int _tmain(int argc, _TCHAR* argv[]){
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData))
		printf("Version is not supported.\n");
	printf("Client started!\n");
	printf("Nhap Thong Tin Server!\n");
	//Step 2: Construct socket
	SOCKET client;
	client = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	//(optional) Set time-out for receiving
	int tv = 10000; //Time-out interval: 10000ms
	setsockopt(client, SOL_SOCKET, SO_RCVTIMEO,
		(const char*)(&tv), sizeof(int));
	//Step 3: Specify server address
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	char ipv4[BUFF_SIZE];
	int port;
	char s;
	printf("IP Server: ");
	gets_s(ipv4,BUFF_SIZE);
	printf("Port: ");
	scanf("%d",&port);
	gets_s(&s,1);
	serverAddr.sin_port = htons((short)port);
	serverAddr.sin_addr.s_addr = inet_addr(ipv4);
	//Step 4: Communicate with server
	char buff[BUFF_SIZE];
	message mess,*mes;
	int ret, count_byte = 0, serverAddrLen = sizeof(serverAddr);
	do {
		//Send message
		printf("Send to server: ");
		gets_s(buff,BUFF_SIZE);
		memcpy(mess.mes, buff, strlen(buff) + 1);
		ret = sendto(client, (char*)&mess, sizeof(mess), 0,
			(sockaddr *)&serverAddr, serverAddrLen);
		if (ret == SOCKET_ERROR) {
			printf("Error! Cannot send mesage.\n");
		}
		else {
			//Count byte sended.
			count_byte += ret;
			printf("Total byte: %d\n", count_byte);
		}
		//Receive echo message
		ret = recvfrom(client, buff, BUFF_SIZE, 0,
			(sockaddr *)&serverAddr, &serverAddrLen);
		if (ret == SOCKET_ERROR) {
			if (WSAGetLastError() == WSAETIMEDOUT)
				printf("Time-out!\n");
			else printf("Error! Cannot receive message.\n");
		}
		else {
			buff[ret] = '\0';
			mes = (message*)buff;
			printf("Receive from server[%s:%d] % s\n",
				inet_ntoa(serverAddr.sin_addr),
				ntohs(serverAddr.sin_port), mes[0].mes);
		}
		strupr(buff);
	} while (strcmp(buff, "BYE") != 0); //end while
	printf("Client complete! Press enter to close!");
	//Step 5: Close socket
	closesocket(client);
	//Step 6: Terminate Winsock
	WSACleanup();
	_getch();
	return 0;
}