#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#define BUFF_SIZE 2048
#pragma comment (lib,"Ws2_32")

struct message {
	int TYPE; // message type
	char mes[1024]; // message
};


int main() {
	//Step1: Inittiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData))
		printf("Version can not support!\n");
	//Step2: Construct socket
	SOCKET server;
	server = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	//Step3: Bind address to socket
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	char ipv4[BUFF_SIZE];
	int port;
	char s;
	int check = 0; //number check
	//input server address
	do{
		printf("IP Server: ");
		gets_s(ipv4,BUFF_SIZE);
		printf("Port: ");
		scanf("%d", &port);
		gets_s(&s, 1);
		serverAddr.sin_port = htons((short)port);
		serverAddr.sin_addr.s_addr = inet_addr(ipv4);
		if (bind(server, (sockaddr *)&serverAddr, sizeof(serverAddr))) {
			printf("Error! Can not bind this address.\n");
			printf("Yeu cau nhap lai!\n");
			check = 1;
		}
		else {
			printf("Server started!\n");
		}
	} while (check == 1);
	sockaddr_in clientAddr;
	char buff[BUFF_SIZE];
	int ret, clientAddrLen = sizeof(clientAddr);
	message *mes;
	while (1) {
		ret = recvfrom(server, buff, BUFF_SIZE, 0, (sockaddr *)&clientAddr, &clientAddrLen);
		if (ret == SOCKET_ERROR) {
			printf("Error : % \n", WSAGetLastError());
		}else {
			buff[ret] = '\0';
			mes = (message*)buff;
			printf("Receiver from client[%s:%d] %s\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port), mes[0].mes);
		}
		ret = sendto(server, buff, ret, 0, (sockaddr *)&clientAddr, clientAddrLen);
		if (ret == SOCKET_ERROR) {
			printf("Error: % \n", WSAGetLastError());
		}
	}//end while
	
	//Close Socket
	closesocket(server);

	//Terminate WinSock
	WSACleanup();
}