#include<stdio.h>
#include<WinSock2.h>
#include<WS2tcpip.h>
#include<tchar.h>
#include "mylibrary.h"
#include <process.h>
#define _CRT_SECURE_NO_WARNINGS
#define SERVER_ADDR "127.0.0.1"
#define BUFF_SIZE 2048

#pragma comment (lib,"Ws2_32")

int receiveData(SOCKET, char *, int, int);
int sendData(SOCKET, char *, int, int);
void prc_mess(state_client *, char *,account *);

account *create_acc(int n) {
	account *acc = new account[n];
	for (int i = 0; i < n; i++) {
		account new_acc;
		strcpy(new_acc.id, "chungnv");
		strcpy(new_acc.psw, "password");
		char c[100];
		sprintf(c, "%i", i);
		strcat(new_acc.id, c);
		strcat(new_acc.psw, c);
		new_acc.failed_login = 0;
		acc[i] = new_acc;
	}
	return acc;
}

int _tmain(int argc, char *argv[]) {

	//Step 1: Initiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData)) {
		printf("Version is not support!\n");
		return 0;
	}
	//Step 2: Contruct socket
	SOCKET listenSock;
	listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	//Step 3: Bind address to socket
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

	//Process command line parameters
	if (argc == 3) {
		if (strcmp(argv[1], "-p") == 0) {
			if (check_number(argv[2])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[2]));
				//Bind
				if (bind(listenSock, (sockaddr*)&serverAddr, sizeof(sockaddr))) {
					printf("ERROR: Can not bind this address!");
					return 0;
				}
				printf("Server started in [%s:%s]", SERVER_ADDR, argv[2]);
			}
			else {
				printf("ERROR : Command line parameter is wrong!");
				return 0;
			}
		}
		else {
			printf("\nERROR : Command line parameter is wrong!");
			return 0;
		}
	}
	else {
		printf("\nERROR : Command line parameter is wrong!");
		return 0;
	}

	//Step 4: Listen request from client
	if (listen(listenSock, 10)) {
		printf("\nError! Cannot listen.");
		_getch();
		return 0;
	}

	//Step 5: Communicate with client
	sockaddr_in clientAddr;
	state_client st_client[FD_SETSIZE];
	SOCKET connSock;
	char buff[BUFF_SIZE];
	int ret, clientAddrlen = sizeof(clientAddr),nEvents;
	fd_set readfds, writefds;
	char rcvBuff[1024], sendBuff[1024];
	account *acc = create_acc(10);
	for (int i = 0; i < FD_SETSIZE; i++) {
		st_client[i].connSock = 0;
		st_client[i].state = "Un-authenticated";
	}
	FD_ZERO(&readfds);
	FD_ZERO(&writefds);
	timeval timeoutInterval;
	timeoutInterval.tv_sec = 10;
	timeoutInterval.tv_usec = 0;

	while (1) {
		FD_SET(listenSock, &readfds);
		for (int i = 0; i < FD_SETSIZE; i++) {
			if (st_client[i].connSock > 0) {
				FD_SET(st_client[i].connSock, &readfds);
			}
		}
		writefds = readfds;
		nEvents = select(0, &readfds, 0, 0, &timeoutInterval);
		if (nEvents < 0) {
			printf("\nError! Cannot poll sockets: %d", WSAGetLastError());
			break;
		}
		if (FD_ISSET(listenSock, &readfds)) { //new client connection
			connSock = accept(listenSock, (sockaddr*)&clientAddr, &clientAddrlen);
			int i;
			for (i = 0; i < FD_SETSIZE; i++) {
				if (st_client[i].connSock == 0) {
					st_client[i].connSock = connSock;
					break;
				}
			}
			if (i == FD_SETSIZE) {
				printf("\nToo many clients");
			}
			FD_CLR(listenSock, &readfds);
			if (--nEvents <= 0) continue; //no more event
		}
		for (int i = 0; i < FD_SETSIZE; i++) {
			if (st_client[i].connSock <= 0) continue;

			if (FD_ISSET(st_client[i].connSock, &readfds)) {
				ret = receiveData(st_client[i].connSock, rcvBuff, 1024, 0);
				if (ret <= 0) {
					FD_CLR(st_client[i].connSock, &readfds);
					closesocket(st_client[i].connSock);
					st_client[i].connSock = 0;
					st_client[i].state = "Un-authenticated";
				}
				else if (ret > 0) {
					prc_mess(&st_client[i],rcvBuff,acc);
					FD_CLR(st_client[i].connSock, &readfds);
				}
				if (--nEvents <= 0) break; //no more event
			}
		}
		
	}

	//Close Socket
	closesocket(listenSock);

	//Terminate WinSock
	WSACleanup();

	_getch();

}
/*The process message function: process message and send answer to client*/
void prc_mess(state_client *state_cl, char *buff,account *acc) {
	state_client st_cl;
	memcpy(&st_cl,state_cl,sizeof(state_client));
	message mes;
	memcpy(&mes, buff, sizeof(message));
	switch (mes.TYPE)
	{
	case 1: { //Specify user
		if (strcmp(st_cl.state, "Un-authenticated") == 0) {
			int check = check_acc(acc, mes.mes, 10);
			if (check == 0) {//not found id
				sendData(st_cl.connSock, "11", 2, 0);
			}
			else {//found id
				sendData(st_cl.connSock, "01", 2, 0);
				st_cl.state = "Specified-user";
				memcpy(&st_cl.acc, &acc[check - 1], sizeof(account));
			}
		}
		else {
			sendData(st_cl.connSock, "21", 2, 0);
		}
		break;
	}
	case 2: {	//Mathed password
		if (strcmp(st_cl.state, "Specified-user") == 0) {
			if (check_pass(mes.mes, st_cl.acc)) {
				sendData(st_cl.connSock, "02", 2, 0);
				st_cl.state = "Authenticated";
			}
			else {
				sendData(st_cl.connSock, "12", 2, 0);
			}
		}
		else {
			sendData(st_cl.connSock, "22", 2, 0);
			
		}
		break;
	}
	case 3: {	//LOGOUT
		if (strcmp(st_cl.state, "Authenticated") == 0) {
			sendData(st_cl.connSock, "03", 2, 0);
			st_cl.state = "Un-authenticated";
		}
		else {
			sendData(st_cl.connSock, "13", 2, 0);
		}
		break;
	}
	default:
		break;
	}
	s
}

/*The recv() wrapper function*/
int receiveData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = recv(s, buff, size, flags);
	if (n <= 0) {
		printf("\nOne client disconnected.");
	}
	else {
		buff[n] = '\0';
		printf("\nreceive one message!");
	}
	return n;
}

/*The send() wrapper function*/
int sendData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = send(s, buff, size, flags);
	if (n <= 0) printf("\nsend error.");
	return n;
}