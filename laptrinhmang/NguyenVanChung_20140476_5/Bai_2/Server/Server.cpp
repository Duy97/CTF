#include <stdio.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include "mylibrary.h"
#include <process.h>
#define SERVER_ADDR "127.0.0.1"
#define BUFF_SIZE 2048

#pragma comment (lib,"Ws2_32")


void sendER(SOCKET);
int receiveData(SOCKET, char *, int, int);
int sendData(SOCKET, char *, int, int);
void prc_mess(state_client *, char *);
int send_to_client(state_client *);
int _tmain(int argc, char *argv[]) {

	//Step 1: Initiate WinSock
	WSADATA wsaData;
	WORD wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData)) {
		printf("Version is not support!\n");
		return 0;
	}
	//Step 2: Contruct socket
	SOCKET listenSock;
	listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	//Step 3: Bind address to socket
	sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

	//Process command line parameters
	if (argc == 3) {
		//check command line parameters
		if (strcmp(argv[1], "-p") == 0) {
			//check port
			if (check_number(argv[2])) {
				serverAddr.sin_port = htons((u_short)atoi(argv[2]));
				//Bind
				if (bind(listenSock, (sockaddr*)&serverAddr, sizeof(sockaddr))) {
					printf("ERROR: Can not bind this address!");
					return 0;
				}
				//Bind success
				printf("Server started in [%s:%s]\n", SERVER_ADDR, argv[2]);
			}
			else {
				printf("ERROR : Command line parameter is wrong!");
				return 0;
			}
		}
		else {
			printf("ERROR : Command line parameter is wrong!");
			return 0;
		}
	}
	else {
		printf("ERROR : Command line parameter is wrong!");
		return 0;
	}

	//Step 4: Listen request from client
	if (listen(listenSock, 10)) {
		printf("Error! Cannot listen.\n");
		_getch();
		return 0;
	}

	//Step 5: Communicate with client
	int nEvents;
	SOCKET connSock;
	fd_set readfds, writefds;
	state_client st_client[FD_SETSIZE];
	sockaddr_in clientAddr;
	int ret, clientAddrlen = sizeof(clientAddr);
	char *rcvbuff = new char[1028];
	for (int i = 0; i < FD_SETSIZE; i++) {
		st_client[i].connSock = 0;
		st_client[i].state = "receive";
		st_client[i].key = 0;
		st_client[i].fp = NULL;
		st_client[i].tem_file = "";
		st_client[i].result_file = "";
	}
	FD_ZERO(&readfds);
	FD_ZERO(&writefds);
	timeval timeoutInterval;
	timeoutInterval.tv_sec = 10;
	timeoutInterval.tv_usec = 0;

	while (1) {
		FD_SET(listenSock, &readfds);
		for (int i = 0; i < FD_SETSIZE; i++) {
			if (st_client[i].connSock > 0) {
				FD_SET(st_client[i].connSock, &readfds);
			}
		}
		writefds = readfds;
		nEvents = select(0, &readfds, &writefds, 0, &timeoutInterval);
		if (nEvents < 0) {
			printf("\nError! Cannot poll sockets: %d", WSAGetLastError());
			break;
		}
		if (FD_ISSET(listenSock, &readfds)) { //new client connection
			connSock = accept(listenSock, (sockaddr*)&clientAddr, &clientAddrlen);
			char *id = inet_ntoa(clientAddr.sin_addr);
			int port = (int)ntohs(clientAddr.sin_port);
			char tem_file[200];
			sprintf(tem_file, "../File_Of_Server/itemporary%i.txt", port);
			char result_file[200];
			sprintf(result_file, "../File_Of_Server/result%i.txt", port);
			int i;
			for (i = 0; i < FD_SETSIZE; i++) {
				if (st_client[i].connSock == 0) {
					st_client[i].connSock = connSock;
					st_client[i].tem_file = tem_file;
					st_client[i].result_file = result_file;
					break;
				}
			}
			if (i == FD_SETSIZE) {
				printf("\nToo many clients");
			}
			FD_CLR(listenSock, &readfds);
			if (--nEvents <= 0) continue; //no more event
		}
		for (int i = 0; i < FD_SETSIZE; i++) {
			if (st_client[i].connSock <= 0) continue;

			if (FD_ISSET(st_client[i].connSock, &readfds)) {
				ret = receiveData(st_client[i].connSock, rcvbuff, 1028, 0);
				if (ret <= 0) {
					FD_CLR(st_client[i].connSock, &readfds);
					closesocket(st_client[i].connSock);
					st_client[i].connSock = 0;
					st_client[i].key = 0;
					st_client[i].state = "receive";
					close_file(st_client[i].fp);
					st_client[i].tem_file = "";
					st_client[i].result_file = "";
				}
				else if (ret > 0) {
					prc_mess(&st_client[i], rcvbuff);//process message received from client
					FD_CLR(st_client[i].connSock, &readfds);
				}
				if (--nEvents <= 0) break; //no more event
				continue;
			}
			if (FD_ISSET(st_client[i].connSock, &writefds)) {
				if (strcmp(st_client[i].state, "sendfile") == 0) {
					ret = send_to_client(&st_client[i]);
					if (ret == 0) {
						st_client[i].state = "receive";
						close_file(st_client[i].fp);
						remove_file(st_client[i].tem_file);
						remove_file(st_client[i].result_file);
					}
				}
				FD_CLR(st_client[i].connSock, &writefds);
				if (--nEvents <= 0) break; //no more event
				continue;
			}
		}

	}


	//close listenSock
	closesocket(listenSock);

	//Terminate WinSock
	WSACleanup();

	//end Server
	_getch();

}

/*The process message function: process message and send answer to client*/
void prc_mess(state_client *st_client, char *buff) {
	state_client st_cl;
	memcpy(&st_cl, st_client, sizeof(state_client));
	message mes;
	memcpy(&mes, buff, sizeof(message));
	switch (atoi(mes.Opcode)) {
	case 0: {
		//create itemporary file and result file
		create_file(st_cl.tem_file);
		create_file(st_cl.result_file);
		st_cl.fp = fopen(st_cl.tem_file, "wb");
		//save key
		st_cl.key = atoi(mes.Payload);
		//change state
		st_cl.state = "ENCODE";
		break;
	}
	case 1: {
		//create itemporary file and result file
		create_file(st_cl.tem_file);
		create_file(st_cl.result_file);
		st_cl.fp = fopen(st_cl.tem_file,"wb");
		//save key
		st_cl.key = atoi(mes.Payload);
		//change state
		st_cl.state = "DECODE";
		break;
	}
	case 2: {
		switch (mes.length)
		{
		case 0: {
			//end receive file
			printf("Receive file from Client: Complete!\n");
			close_file(st_cl.fp);
			close_file(st_client[0].fp);
			if (strcmp(st_cl.state, "ENCODE") == 0) {//ENCODE file
				if (encode_file(st_cl.result_file, st_cl.tem_file, st_cl.key)) st_cl.state = "sendfile";
				else sendER(st_cl.connSock);
			}
			else {//DECODE file
				if (encode_file(st_cl.result_file, st_cl.tem_file, (-st_cl.key))) st_cl.state = "sendfile";
				else sendER(st_cl.connSock);
			}
			st_cl.fp = fopen(st_cl.result_file, "rb");//open result file
			break;
		}
		default: {
			//save_file
			fwrite((byte*)mes.Payload, 1, (int)mes.length, st_cl.fp);
			fflush(st_cl.fp);
			break;
		}
		}
	}
	case 3: {
		break;
	}
	default:
		break;
	}
	memcpy(st_client, &st_cl, sizeof(state_client));
}

/*send file to server*/
int send_to_client(state_client *st_client) {
	state_client st_cl;
	memcpy(&st_cl, st_client, sizeof(state_client));
	message mes;
	memcpy(mes.Opcode, "2", 1);
	int read;
	read = fread_s(mes.Payload, 1024, sizeof(char), 1024, st_cl.fp);
	mes.length = read;
	sendData(st_cl.connSock, (char *)&mes, 1028, 0);
	memcpy(st_client, &st_cl, sizeof(state_client));
	return read;
}

//Send message notify Error
void sendER(SOCKET connsock) {
	message meserr;
	int ret;
	memcpy(meserr.Opcode, "3", 1);
	ret = send(connsock, (char *)&meserr, sizeof(message), 0);
	if (ret == SOCKET_ERROR) {
		printf("ERROR: %", WSAGetLastError());
	}
	return;
}


/*The recv() wrapper function*/
int receiveData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = recv(s, buff, size, flags);
	if (n <= 0) {
		printf("\nOne client disconnected.");
	}
	else {
		buff[n] = '\0';
	}
	return n;
}

/*The send() wrapper function*/
int sendData(SOCKET s, char *buff, int size, int flags) {
	int n;
	n = send(s, buff, size, flags);
	if (n <= 0) printf("\nsend error.");
	return n;
}