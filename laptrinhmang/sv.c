#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>


int compare_strings(char a[], char b[])
{
    int c = 0;
    while (a[c] == b[c]) 
    {
        if (a[c] == '\0' || b[c] == '\0')
        break;
        c++;
    }
    if (a[c] == '\0' && b[c] == '\0')
    return 0;
    else
    return -1;
}

int main() {
    //khai bao may chu va 2 may khach
    int welcomeSocket, Client1, Client2;
    struct sockaddr_in serverAddr;
    struct sockaddr_storage serverStorage;
    socklen_t addr_size;
    char buffer[1024];

    welcomeSocket = socket(PF_INET, SOCK_STREAM, 0);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(7891);
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);
    bind(welcomeSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));

    
    if (listen(welcomeSocket,5)==0)
        printf("Listening\n");
    else
        printf("Error\n");

    //lien ket voi 2 may khach
    addr_size = sizeof serverStorage;
    Client1 = accept(welcomeSocket, (struct sockaddr *) &serverStorage, &addr_size);
    Client2 = accept(welcomeSocket, (struct sockaddr *) &serverStorage, &addr_size);

    int cmdEXIT = 0;
    
    while (cmdEXIT == 0)
    {
        //nhan tin nhan tu client1
        recv(Client1, buffer, 1024, 0);
        //gui no den client2
        printf ("%s\ngui den  Client2\n", buffer);
        send(Client2,buffer,1024,0);
        //thoat khoi vong lap neu client 1 gui exit
        if (compare_strings(buffer, "exit")==0)
        {   
            cmdEXIT = 1;
        }
        
        else 
        {
            //lam rong bo dem
            memset(&buffer[0], 0, sizeof(buffer));
            //nhan tin nhan tu client2   
            recv(Client2, buffer, 1024, 0);
            //gui cho client1
            printf ("%s\ngui den Client1\n", buffer);
            send(Client1,buffer,1024,0);
            //thoat neu client2 gui exit
            if (compare_strings(buffer, "exit")==0)
            {
                cmdEXIT = 1;
            }
        }
    }

    return 0;
}