pubKey = open("pubkey.txt", 'rb').read().replace(' ', '').replace('L','').split(',')
nbit = len(pubKey)
encoded = '10645553923178583890634245062599599958777032719084904069175071172992997795'
from Crypto.Util.number import *
import numpy.matrix 

A = matrix(nbit+1,nbit+1)
for i in xrange(nbit):
    A[i,i] = 1
for i in xrange(nbit):
    A[i,nbit] = pubKey[i]
A[nbit,nbit] = -int(encoded)

res = A.LLL().str().split('\n')

def check(arr):
    for i in arr:
        if i in '01':
            continue
        else:
            return False
    return True

def decode(binary):
    flag = ''
    for i in range(0,len(binary),8):
        flag += chr(int(binary[i:i+8],2))
    return flag

for line in res:
    flag = line[1:-1].replace(' ', '')
    if check(flag):
        print decode(flag)

