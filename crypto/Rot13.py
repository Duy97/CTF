import string 
rot13 = string.maketrans(
	"NOPQRSTUVWXYZnopqrstuvwxyzABCDEFGHIJKLMabcdefghijklm",
    "ABCDEFGHIJKLMabcdefghijklmNOPQRSTUVWXYZnopqrstuvwxyz" )
x = open('./MinionQuest.pdf','rb').read()
y = open('./out.pdf','w')
y.write(string.translate(x,rot13))
print'done'