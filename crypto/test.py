from collections import defaultdict

def repeated_block(buffer,block_length=16):
	reps = defaultdict(lambda: -1)
	
	for i in range (0,len(buffer),block_length):
		block = bytes(buffer [i:i + block_length])
		reps[block] +=1

	return sum(reps.values())
cipher = " pham van duy  nguyen van phuc pham van duy"
c= repeated_block(bytearray(cipher))
print c
