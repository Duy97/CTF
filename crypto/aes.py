
from Crypto.Cipher import AES

BS =16
pad = lambda s:s+(BS - len(s) %BS)*char(BS -len(s)%BS)
unpad = lambda s: s[0:-ord(s[-1])]

def decrypt_cbc(key,ciphertext):
	iv = ciphertext[:16]
	cipher = AES.new(key,AES.MODE_CBC,iv)
	return unpad(cipher.decrypt(ciphertext[16:]))



cipher1 = '4ca00ff4c898d61e1edbf1800618fb2828a226d160dad07883d04e008a7897ee2e4b7465d5290d0c0e6c6822236e1daafb94ffe0c5da05d9476be028ad7c1d81'.decode("hex")
key1= '140b41b22a29beb4061bda66b6747e14'.decode("hex")

print decrypt_cbc(key1,cipher1)

cipher2 = '5b68629feb8606f9a6667670b75b38a5b4832d0f26e1ab7da33249de7d4afc48e713ac646ace36e872ad5fb8a512428a6e21364b0c374df45503473c5242a253'.decode("hex")
key2 = '140b41b22a29beb4061bda66b6747e14'.decode("hex")

print decrypt_cbc(key2, cipher2)

# AES MODE CTR
def decrypt_ctr	(key,iv,data):
	crypt = AES.new(key,AES.MODE_CTR,counter=lambda: iv)
	return crypt.decrypt(data)
key3 = '36f18357be4dbd77f050515c73fcf9f2'.decode("hex")
cipher3 = '69dda8455c7dd4254bf353b773304eec0ec7702330098ce7f7520d1cbbb20fc388d1b0adb5054dbd7370849dbf0b88d393f252e764f1f5f7ad97ef79d59ce29f5f51eeca32eabedd9afa9329'

iv1 = cipher3[:32].decode("hex")
iv2 = hex(int(cipher3[:32],16)+1)[2:-1].decode("hex")
iv3 = hex(int(cipher3[:32],16)+2)[2:-1].decode("hex")
iv4 = hex(int(cipher3[:32],16)+3)[2:-1].decode("hex")

ct1 = cipher3[32:64].decode("hex")
ct2 = cipher3[64:96].decode("hex")
ct3 = cipher3[96:128].decode("hex")
ct4 = cipher3[128:].decode("hex")

print decrypt_ctr(key3,iv1,ct1) +decrypt_ctr(key3,iv2,ct2)+decrypt_ctr(key3,iv3,ct3)+decrypt_ctr(key3,iv4,ct4)


key4 = '36f18357be4dbd77f050515c73fcf9f2'.decode("hex")
cipher4 = '770b80259ec33beb2561358a9f2dc617e46218c0a53cbeca695ae45faa8952aa0e311bde9d4e01726d3184c34451'
iv1 = cipher4[:32].decode("hex")
iv2 =hex(int(cipher4[:32],16)+1)[2:-1].decode("hex")
iv3 = hex(int(cipher4[:32],16)+2)[2:-1].decode('hex')
iv4 = hex(int(cipher4[:32],16)+3)[2:-1].decode('hex')
ct1 = cipher4[32:64].decode("hex")
ct2 = cipher4[64:96].decode("hex")
ct3 = cipher4[96:128].decode('hex')
ct4 = cipher4[128:].decode("hex")
print decrypt_ctr(key4,iv1,ct1)+decrypt_ctr(key4,iv2,ct2)+decrypt_ctr(key4,iv3,ct3)+decrypt_ctr(key4,iv4,ct4)















