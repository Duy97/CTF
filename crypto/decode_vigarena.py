import itertools
import hashlib

chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ{}'
def add(a,b):
	ia = chars.index(a)
	ib = chars.index(b)
	ic = (ia+ib) % len(chars)
	return chars[ic]
def sub(a,b):
	ia = chars.index(a)
	ib = chars.index(b)
	ic = (ia -ib) %len(chars)
	return chars[ic]
def encrypto(plain,key):
	cipher = ''
	for i in range(len(plain)):
		cipher += add(plain[i],key[i%len(key)])
	return cipher
def decrypto(cipher,key):
	plain = ''
	for i in range(len(cipher)):
		plain += sub(cipher[i] , key[i%len(key)])
	return plain
def find_key(cipher,plain,key_length):
	key = ''
	for i in range(key_length):
		key += sub(cipher[i] ,plain[i])
	return key
def md5(plain):
	return hashlib.md5(plain)
print find_key('LMIG}RP','SECCON{',len('SECCON{'))
key =''
cipher = 'LMIG}RPEDOEEWKJIQIWKJWMNDTSR}TFVUFWYOCBAJBQ'
for i in itertools.product(chars, repeat=5):
	key += 'VIGENER'+ ''.join(i)
	m = decrypto(cipher,key)
	if md5(m) == 'f528a6ab914c1ecf856a1d93103948fe':
		print 'flag: ', m
		break






