from collections import defaultdict

def repeated_block(buffer,block_length=16):
	reps = defaultdict(lambda: -1)
	for i in range (0,len(buffer),block_length):
		block = bytes(buffer [i:i + block_length])
		reps[block] +=1
	return sum(reps.values())

max_reps =  0
ecb_cipher =None
for cipher in list(open("8.txt","r")):
	cipher = cipher.rstrip()
	reps = repeated_block(bytearray(cipher))
	print reps
	if reps > max_reps:
		max_reps = reps
		ecb_cipher = cipher
print ecb_cipher

