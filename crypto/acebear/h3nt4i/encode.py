from Crypto.Util.number import *
from gmpy2 import *
flag = open("flag.txt","r").read()
attacker = bytes_to_long(flag)

def FBI_warning(sora,intruder):
    return sora**2 + 2*intruder**2 

def generator():
    j = getPrime(1024)
    intruder = 0
    while 1:
	intruder = intruder + 1
	a = j + intruder
  	if (isPrime(a)):
	   v = FBI_warning(j,intruder) - intruder*a 
	   if isPrime(v):
	      break
    return j,a,v

j,a,v = generator()
bk = a*v
soe = 65537
star = pow(attacker,soe,bk)
c = pow(star,invert(soe,(a-1)*(v-1)),bk)
f = open("flag.enc","w").write("j = " + str(j) + "\nbk = " + str(bk) + "\nstar = " + str(star) + "\nsoe = 65537" )
 




