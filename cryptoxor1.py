
def xor_string(s, t):
	if isinstance(s, str):
		 return "".join(chr(ord(a) ^ ord(b)) for a,b in zip(s,t))
	else:
		return bytes([a^b for a,b in zip(s,t)])
ciphertext = '32352b3521362719200c0d150a04103d330711510315530f053d3952131f'
key = 'PTITCTF'
decrypto = xor_string(ciphertext, key)
print decrypto