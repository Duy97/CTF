def caesar(plaitext,shift):
	import string 
	alphabet = string.ascii_lowercase
	shift_alphabet = alphabet[shift:]+alphabet[:shift]
	Alphabet = string.ascii_uppercase
	shift_Alphabet = Alphabet[shift:] + Alphabet[:shift]
	numbers = '0123456789'
	shift_number = numbers[shift:] + numbers[:shift]

	table = string.maketrans(alphabet+Alphabet+numbers,shift_alphabet+shift_Alphabet+shift_number)
	return plaitext.translate(table)
plaitext = 'CnozkNgz{18571g0397188h59279l30g18j82l2g28gi4i1hj}'
for i in range(26):
	print caesar(plaitext,i)
