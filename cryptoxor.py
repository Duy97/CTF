from os import urandom
def genkey(length):
	return urandom(length)
def xor_string(s, t):
	if isinstance(s, str):
		 return "".join(chr(ord(a) ^ ord(b)) for a,b in zip(s,t))
	else:
		return bytes([a^b for a,b in zip(s,t)])
message = 'Burning em, if you aint quick and nimbleI go crazy when I hear a cymbal'
print message
key = genkey(len(message))
print key
ciphertext = xor_string(message.encode('utf8'), key)
print ciphertext
decrypto = xor_string(ciphertext, key)
print decrypto