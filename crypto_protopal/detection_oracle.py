from random import randint

from Crypto.Cipher import AES
def random_key(length):
	key = bytearray(length)
	for i in range(length):
		key[i] = chr(randint(0,255))
	return key
def xor(a,b):
	c = bytearray(len(a))
	for i in range(len(a)):
		c[i] = a[i] ^b[i]
	return c
def pad_pkcs7(buffer, block_size):
    if len(buffer) % block_size:
        padding = (len(buffer) / block_size + 1) * block_size - len(buffer)
    else:
        padding = 0
    # Padding size must be less than a byte
    assert 0 <= padding <= 255
    new_buffer = bytearray()
    new_buffer[:] = buffer
    new_buffer += bytearray([chr(padding)] * padding)
    return new_buffer
def aes_ecb_enc(buffer,key):
	obj =AES.new(key,AES.MODE_ECB)
	return bytearray(obj.encrypt(bytes(buffer)))
def aes_cbc_enc(buffer,key,iv):
	plaintext = pad_pkcs7(buffer,AES.block_size)
	cipher = bytearray(len(plaintext))
	prev_block = iv 
	for i in range(0,len(plaintext),AES.block_size):
		cipher[i:i+AES.block_size] = aes_ecb_enc(xor(plaintext[i:i+AES.block_size],prev_block),key)
		prev_block = plaintext[i:i+AES.block_size]
	return cipher
def encrypt_oracle(buffer):
	bytes_to_add = randint(5,10)
	plaintext = pad_pkcs7(random_key(bytes_to_add)+buffer+random_key(bytes_to_add),AES.block_size)
	key = bytes(random_key(16))
	if randint(0,1):
		return aes_ecb_enc(plaintext,key),1
	else:
		iv = random_key(16)
		return aes_cbc_enc(plaintext,key,iv),0
def repeated_blocks(buffer,block_length =16):
	reps  = defaultdict(lambda:-1)
	for i in range(0,len(buffer),block_length):
		block = bytes(buffer[i:i+block_length])
		reps[block] +=1
		return sum(reps.values())
def is_ecb_mode(buffer,block_size):
	return repeated_blocks(buffer,block_size)>0
plaintext = bytearray(''.join(list(open('11.txt','r'))))
for i in range(1000):
	cipher,ecb_mode = encrypt_oracle(plaintext)
	if ecb_mode != is_ecb_mode(cipher,AES.block_size):
		print 'detetion does not working'
		exit()
	
print 'ok'


















	

