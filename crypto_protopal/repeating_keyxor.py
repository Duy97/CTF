plain = """Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal"""
a = len(plain)/3 +len(plain)%3
cipher=''
key = 'ICE' * a
for i in range(len(plain)):
	cipher += chr(ord(plain[i]) ^ ord(key[i]))
cipher = cipher.encode("hex")
print cipher