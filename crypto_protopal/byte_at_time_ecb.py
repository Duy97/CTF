from Crypto.Cipher import AES
from random import randint
from collections import defaultdict

def pad_pksc7(buffer,block_size):
	if len(buffer) % block_size:
		padding = (len(buffer)/block_size +1) *block_size -len(buffer)
	else:
		padding =0
	assert 0<=padding<=255
	new_buffer = bytearray()
	new_buffer [:]=buffer
	new_buffer += bytearray([chr(padding)] *padding)
	return new_buffer
def random_key(length):
	key = bytearray(length)
	for i in range(length):
		key[i]=chr(randint(0,255))
	return key
key = bytes(random_key(16))
def aes_ecb_enc(buffer,key):
	obj = AES.new(key,AES.MODE_ECB)
	return bytearray(obj.encrypt(bytes(buffer)))
def encryption_oracle(data):
	unknow_string = bytearray((' 2byJ4S+X4pvCdLgC+WhDVmN7wXMCr4hhivEp3xjIy0O8JtPPgyl5edbfzYQUDA4i').decode('base64'))
	plaintext = pad_pksc7(data +unknow_string,AES.block_size)
	return aes_ecb_enc(plaintext,key)
def get_block_size(oracle):
	Cipher_length= len(oracle(bytearray()))
	i = 1
	while True:
		data= bytearray('A'*i)
		new_cipher_length = len(oracle(data))
		block_size = new_cipher_length - Cipher_length
		if block_size:
			return block_size
		i+=1
def repeated_blocks(buffer,block_length =16):
	reps = defaultdict(lambda: -1)
	for i in range(0,len(buffer),block_length):
		block = bytes(buffer[i:i+block_length])
		reps[block] +=1
	return sum(reps.values())
def is_ecb_mode(buffer,block_size):
	return repeated_blocks(buffer,block_size)>0
def get_unknow_string_size(oracle): 
	Cipher_length = len(oracle(bytearray()))
	i = 1
	while  True:
		data= bytearray('A'*i)
		new_cipher_length = len(oracle(data))
		if Cipher_length !=new_cipher_length:
			return new_cipher_length -i
		i+=1
def get_unknow_string(oracle):
	block_size = get_block_size(oracle)
	is_ecb = is_ecb_mode(oracle(bytearray('YELLOW SUBMARINE' *2)),block_size)
	assert is_ecb
	unknow_string_size = get_unknow_string_size(oracle)
	unknow_string = bytearray()
	unknow_string_size_rounded = (((unknow_string_size/block_size )+1) *block_size)
	for  i in range(unknow_string_size_rounded -1,0,-1):
		d1 = bytearray('A'*i)
		c1 = oracle(d1)[:unknow_string_size_rounded]
		for c in range(256):
			d2 = d1[:] + unknow_string+chr(c)
			c2 = oracle(d2)[:unknow_string_size_rounded]
			if c1==c2:
				unknow_string += chr(c)
				break
	return unknow_string
print get_unknow_string(encryption_oracle)

































