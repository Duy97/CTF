def xor(b1,b2):
	b=bytearray(len(b1))
	for i in range(len(b1)):
		b[i] = b1[i] ^ b2[i]
	return b
freqs = {
    'a': 0.0651738,
    'b': 0.0124248,
    'c': 0.0217339,
    'd': 0.0349835,
    'e': 0.1041442,
    'f': 0.0197881,
    'g': 0.0158610,
    'h': 0.0492888,
    'i': 0.0558094,
    'j': 0.0009033,
    'k': 0.0050529,
    'l': 0.0331490,
    'm': 0.0202124,
    'n': 0.0564513,
    'o': 0.0596302,
    'p': 0.0137645,
    'q': 0.0008606,
    'r': 0.0497563,
    's': 0.0515760,
    't': 0.0729357,
    'u': 0.0225134,
    'v': 0.0082903,
    'w': 0.0171272,
    'x': 0.0013692,
    'y': 0.0145984,
    'z': 0.0007836,
    ' ': 0.1918182 
}
def score(s):
	score=0
	for i in s:
		c = i.lower()
		if c in freqs:
			score+= freqs[c]
	return score
def breaksingeByteXor(b1):
	max_socre=0
	decipher=''
	for i in range(256):
		b2 = [i] * len(b1)
		plaintext = bytes(xor(b1,b2))
		pscore = score(plaintext)
		if pscore>max_socre :
			max_socre =pscore
			decipher=plaintext
			key = chr(i)
	return key,decipher
b1 = bytearray.fromhex("fbf9eefce1f2f5eaffc5e3f5efc5efe9fffec5fbc5e9f9e8f3eaeee7")
print b1
print breaksingeByteXor(b1)












