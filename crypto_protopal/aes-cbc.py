from Crypto.Cipher import AES
from binascii import a2b_base64
from Crypto.Util.strxor import strxor

def ecb_encrypt(data,key):
	cipher = AES.new(key,AES.MODE_ECB)
	return cipher.encrypt(data)
def ecb_decrypt(data,key):
	decipher = AES.new(key,AES.MODE_ECB)
	return decipher.decrypt(data)
def pad(data,final_len=None):
	if final_len ==None:
		final_len =(len(data)/16 +1)*16
	padding_len = final_len -len(data)
	return data+chr(padding_len)*padding_len
def unpad(data):
	padding_len= ord(data[len(data)-1])
	for i in range(len(data)-padding_len,len(data)):
		if ord(data[i]) !=padding_len:
			return data
	return data[:-padding_len]
def xor(a,b):
	return ''.join(chr(ord(x) ^ ord(y)) for x,y in zip(a,b) )
def cbc_encrypt(data,key,iv):
	data = pad(data)
	block_count = len(data)/16
	encrypt_data = ''
	prev_block = iv
	for b in range(block_count):
		cur_block = data[b*16:(b+1)*16]
		encrypt_block = ecb_encrypt(xor(cur_block,prev_block),key)
		encrypt_data +=encrypt_block
		prev_block = encrypt_block
	return encrypt_data
def cbc_decrypt(data,key,iv):
	block_count = len(data)/16
	decrypt_data = ''
	prev_block = iv
	for b in range(block_count):
		cur_block = data[b*16:(b+1)*16]
		decrypt_block = ecb_decrypt(cur_block,key)
		decrypt_data += xor(decrypt_block,prev_block)
		prev_block = decrypt_block
	return unpad(decrypt_data)
file = open('cbc.txt','r').read()
data = a2b_base64(''.join(line.strip() for line in file))
key  = 'YELLOW SUBMARINE'
iv = '\x00'*16
a= cbc_decrypt(data,key,iv)
b = cbc_encrypt(a,key,iv).encode('base64')+'=='
x= file.replace('\n','').replace(' ','')
if x==b:
	print 'duy'






































